---
layout: handbook-page-toc
title: "Digital Experience Handbook"
description: "Learn more about the Digital Experience purpose, vision, mission, objective and more in this handbook."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Digital Experience Handbook

## Overview

### Managed Channels
[The Marketing Website](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/)

### Purpose

*Why we exist*

To guide the intention of GitLab’s initial impact. Our goal is to continually shift perception by delivering positive experiences at velocity.

### Vision

*Where we're going*

A future where **everyone can contribute**, consumers become contributors and we greatly increase the rate of human progress.

### Mission

*What we do*

Create tools and processes that unlock GitLab team members and users to deliver results.

### Objective

Contribute and execute on the strategy for the marketing site.

1. Achieve traffic goals (inbound SEO)
2. Achieve MQL goals (conversion rate)
3. Support broader marketing initiatives w/ deliverables

### Strategy (FY21 Q3)

1. Implement a CMS - so that the content marketing team is more productive, and more teams can self-serve improvements to the website
2. Create a Design System - developer productivity, more consistent up-leveled look/feel of the website
3. Set the long-term strategy for website to continue to see the content, design, and processes refreshed
4. Establish SEO partnership

View long-term goals here: [https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/foundations-agenda/](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/foundations-agenda/)

### Principles (WIP)

1. **Understand** We need to understand the challenge before we can solve it (this could be related to process, it’s asking questions)
2. **Trust** We trust our process (we have a process, we have versions of our process based on scope) We trust each other
    1. **Respect**
    2. **Transparency**
3. **Inclusive** Add description - make sure this is sharp
    1. **Empathy** we creating things for our customers/users
    2. **Supportive** We support each other and other teams to deliver the best results (something about creating safety and educating others) psychological safety
        1. Provide feedback to support each other
    3. **Accessible**
4. **Intentional** simple over complicated
    1. Meet people where they’re at/**context**/cognitive load rewrite this
5. **Opinion** We have a strong opinion that differentiates us
6. **Results** We focus on results (something about keep it simple, measurement)
    1. **Actionable**
    2. **Measurable**
7. **Iteration** When applying the [MVC](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc) approach, make things smaller by reducing the scope of the job-to-be-done rather than sacrificing the end goal.
    1. **Experimentation** We’re driven by experimentation and judge success with data (I dunno, something from essential experiences)
    2. **Increments**  We take large tasks and break them down into multiple iterative tasks

### What We Are Not

[COMING]

## Team

| Team Member                                                                                             | Role                               | Contact Info                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------- | ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| [**Michael Preuss**](https://about.gitlab.com/company/team/#mpreuss22)   | Senior Manager, Digital Experience | - Email: [mpreuss@gitlab.com](mailto:mpreuss@gitlab.com)<br>- GitLab: [@mpreuss22](https://gitlab.com/mpreuss22)<br>- Slack: @mpreuss22<br>- ReadMe: [michael-preuss](https://about.gitlab.com/handbook/marketing/readmes/michael-preuss.html)      |
| [**Lauren Barker**](https://about.gitlab.com/company/team/#laurenbarker) | Senior Full Stack Developer        | - Email: [lbarker@gitlab.com](mailto:lbarker@gitlab.com)<br>- GitLab: [@laurenbarker](https://gitlab.com/laurenbarker)<br>- Slack: @lbarker<br>- ReadMe: [lauren-barker](https://about.gitlab.com/handbook/marketing/readmes/lauren-barker.html)  |
| [**Brandon Lyon**](https://about.gitlab.com/company/team/#brandon_lyon)  | Frontend Engineer                  | - Email: [blyon@gitlab.com](mailto:blyon@gitlab.com)<br>- GitLab: [@brandon_lyon](https://gitlab.com/brandon_lyon)<br>- Slack: @Brandon Lyon<br>- ReadMe: [brandon-lyon](https://about.gitlab.com/handbook/marketing/readmes/brandon-lyon.html) |
| [**Tyler Williams**](https://about.gitlab.com/company/team/#tywilliams)  | Website Full Stack Developer                  | - Email: [tywilliams@gitlab.com](mailto:tywilliams@gitlab.com)<br>- GitLab: [@tywilliams](https://gitlab.com/tywilliams)<br>- Slack: @Tyler Williams<br>- ReadMe: [tyler-williams](https://about.gitlab.com/handbook/marketing/readmes/tyler-williams.html) |
| [**Laura Duggan**](https://about.gitlab.com/company/team/#lduggan)  | Frontend Engineer                  | - Email: [lduggan@gitlab.com](mailto:lduggan@gitlab.com)<br>- GitLab: [@lduggan](https://gitlab.com/lduggan)<br>- Slack: @Laura Duggan<br>- ReadMe: [laura-duggan](https://about.gitlab.com/handbook/marketing/readmes/laura-duggan.html) |
| Stephen McGuinness                                                                                      | Design System Lead (Contract)      | - GitLab: [@smcguinness1](http://smcguinness1)<br>- Slack: @Stephen McGuinness                                                               |
| Jessica Halloran                                                                                        | UX Designer (Contract)             | - GitLab: [@jhalloran](https://gitlab.com/jhalloran)<br>- Slack: @jhalloran                                                                  |
| Tina Lise Ng                                                                                            | UX Designer (Contract)             | - GitLab: [@Tinaliseng](https://gitlab.com/Tinaliseng)<br>- Slack: @Tina Lise Ng                                                             |
| Javier Garcia                                                                                           | Frontend Engineer (Contract)       | - GitLab: [@jgarc257](https://gitlab.com/jgarc257)<br>- Slack: @Javi                                                                         |
| Sanmi Ayotunde                                                                                          | Frontend Engineer (Contract)       | - GitLab: [@sanmiayotunde](https://gitlab.com/sanmiayotunde)<br>- Slack: @sanmiayotunde <br>- ReadMe: [sanmiayotunde](https://about.gitlab.com/handbook/marketing/readmes/sanmiayotunde.html)                                        |

## Metrics

Specific targets can be found [here](/handbook/marketing/inbound-marketing/#goals)

### North Star Metric

**Metric**: Inbound marketing qualified leads.

### Supporting Metrics

There are many other digital marketing metrics we track for the website. These include pageviews, unique visitors, time on site, and conversion metrics for key funnels. These detailed metrics are leading indicators for the health of our North Start metric, and are internal to GitLab.

## Scope

The GitLab digital marketing platform, or simply the “GitLab Website” refers to all of the content on `https://about.gitlab.com` except the [blog](/handbook/marketing/blog/) and [handbook](/handbook/).

See [Where should content go?](/handbook/git-page-update/#where-should-content-go) to learn which web property is the most appropriate place to put different types of content. To learn what section of the website different content belongs see [definitions](about:blank#definitions).

The website does not include - The Docs: `docs.gitlab.com` - The GitLab.com product: `gitlab.com` - The Handbook: `about.gitlab.com/handbook` - The blog: `about.gitlab.com/blog`

## OKRs

We collaboratively define OKRs as a team with cross functional partners in advance of each quarter. Once OKR candidates are complete the Growth Marketing Leads review and submit for ratification.

### FY21 Q4 OKRs

**Objective:** [Q4 FY21 Growth Marketing OKR: Implement 3rd Party CMS on about.gitlab.com](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/153)

**Key Result:** During Q3 FY21, we made the decision to move forward with a 3rd party CMS (content management system) for the GitLab marketing website. A vendor selection process was completed, and the decision is to implement NetlifyCMS.

**Objective:** [Q4 FY21 Digital Experience OKR: Marketing Design System
](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/92)

**Key Result:** During Q3 FY21, we will invest in planning, designing, and building a design system for our Marketing site. The Marketing Design System will increase our efficiency and make it easier for everyone to contribute.

## Our Process

Our goal is to deliver a release every 2 weeks. Our release day is always a Wednesday. We can push MRs at any time but for collaborative work initiates we plan a package for delivery to ensure we're improving our customer's experience rapidly.

### Sprint Cycle

For FY21 Q4 we have 2 sprint cycles running which start on alternate weeks.

<figure class="video_container">
    <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FVancouver&amp;src=Y185Z2FiaTg1cmYxMTdnOGhhNDhya2tmbzUxY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Y19pcnFkNzNwNnA0YnJ1a2prOXUyZjF0NGNxMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23F4511E&amp;color=%23B39DDB&amp;showTitle=0&amp;showNav=1&amp;showDate=1&amp;showPrint=0&amp;showTabs=1&amp;showCalendars=1" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

### Sprint Planning

Before a sprint starts, our team aligns on what will be delivered during the sprint. We identify issues that need to be written, write them, then add them to our boards. We weight these issues and target delivering 10 weight points per sprint.

This is the template we use for [Sprint Planning](https://docs.google.com/document/d/19kuMa9Jx5k9DTqCmeLYmGOnG_i2MnDr2zqHWsucY4Ew/edit)

<figure class="video_container">
    <iframe src="https://docs.google.com/document/d/19kuMa9Jx5k9DTqCmeLYmGOnG_i2MnDr2zqHWsucY4Ew/edit"></iframe>
</figure>

### Sprint Retros

Sprint Retros allow us to reflect on what went well and what we can to do continuously improve our process.

This is the template we use for [Sprint Retros](https://docs.google.com/document/d/1KSds3hjnJ250WWWZ9_uNpU4RYQA82mF3ylAcpsf7fGE/edit)

<figure class="video_container">
    <iframe src="https://docs.google.com/document/d/1KSds3hjnJ250WWWZ9_uNpU4RYQA82mF3ylAcpsf7fGE/edit"></iframe>
</figure>

**Marketing Design System Sprint Planning and Retros**

[https://drive.google.com/drive/u/0/folders/1E0101X1-eYirolSxz-Q96HIdHFJhzzOK](https://drive.google.com/drive/u/0/folders/1E0101X1-eYirolSxz-Q96HIdHFJhzzOK)

**CMS Sprint Planning and Retros**

[https://drive.google.com/drive/folders/1req1RC4tdOgKrn-bZEvS5G2nKwuymIKy](https://drive.google.com/drive/folders/1req1RC4tdOgKrn-bZEvS5G2nKwuymIKy?usp=sharing)

**Weighting**

We use issue weight to plan and manage our work. Team members are not held to weights, they’re simply a planning and learning tool. Digital Experience weights are based on the following point scale: 1pt = .5 day


## Contact Us

### Slack Group

**@digital-experience** use this handle in any channel to mention every member of our team.

### Slack Channels

[#digital-experience-team](https://gitlab.slack.com/archives/CN8AVSFEY)

[#inbound-mktg](https://gitlab.slack.com/archives/C012U3CASJ2)

[#marketing](https://gitlab.slack.com/archives/C0AKZRSQ5)

[#website](https://gitlab.slack.com/archives/C62ERFCFM)

## GitLab Unfiltered Playlists

**Digital Experience**

[https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba)

## Requesting Support

[https://about.gitlab.com/handbook/marketing/inbound-marketing/#requesting-support](https://about.gitlab.com/handbook/marketing/inbound-marketing/#requesting-support)

## How we use GitLab

[https://about.gitlab.com/handbook/marketing/inbound-marketing/#how-we-use-gitlab](https://about.gitlab.com/handbook/marketing/inbound-marketing/#how-we-use-gitlab)
