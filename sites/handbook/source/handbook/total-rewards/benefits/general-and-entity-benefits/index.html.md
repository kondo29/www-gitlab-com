---
layout: handbook-page-toc
title: General & Entity Specific Benefits
description: A list of the General & Entity Specific Benefits that GitLab offers.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and team members, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to team members in those countries only via our entity specific benefits. GitLab has also made provisions for Parental Leave which may apply to team members and contractors but this may vary depending on local country laws. If you are unsure please reach out to the Total Rewards team.

## Contact Total Rewards
For any questions relating to benefits, please reach out to [Total Rewards](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).

## Entity Benefits
- [GitLab BV (Netherlands)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium)
- [Global Upside (India and Philippines)](/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india)
- [GitLab Inc (US)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia & New Zealand)](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia)
- [GitLab Canada Corp](/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits)
- [GitLab GK (Japan)](/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-gk)
- [GitLab Ireland LTD](/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-ireland-ltd)
- [Remote Technology (South Africa, Mexico, Hungary and Austria)](/handbook/total-rewards/benefits/general-and-entity-benefits/remote-com)
- [Safeguard (Spain, Italy, France, Brazil and Switzerland)](/handbook/total-rewards/benefits/general-and-entity-benefits/safeguard/)

### Benefits Available to Contractors

Contractors of GitLab BV are eligible for the [general benefits](/handbook/total-rewards/benefits/#general-benefits), but are not eligible for entity specific benefits. A contractor may bear the costs of their own health insurance, social security taxes, payroll administration, or tax details. 

Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.


## Spending Company Money

GitLab will [pay for the items you need to get your job done](/handbook/spending-company-money).

## Stock Options

[Stock options](/handbook/stock-options/) are offered to most GitLab team members. We strongly believe in team member ownership in our Company. We are in business to create value for our shareholders and we want our team members to benefit from that shared success.

## Life Insurance

In the unfortunate event that a GitLab team member passes away, GitLab will provide a [$20,000](/handbook/total-rewards/compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse, partner, family member, friend, or charity.
* This benefit is not applicable if there is a separate life insurance policy as part of the [Entity Specific Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits) package.
* For all other GitLab team members, the following conditions apply:
  * The team member must be either an employee or direct contractor.
  * The team member must have indicated in writing to whom the money should be transferred. To do this you must complete the [Expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize. Sign, save as a pdf and upload to your Employee Uploads folder in BambooHR.
  * For part-time GitLab team members, the lump sum is calculated pro-rata, so for example for a team member that works for GitLab 50% of the time, the lump sum would be [$10,000](/handbook/total-rewards/compensation/#exchange-rates).

## Paid Time Off

GitLab has a "no ask, must tell" [time off policy](/handbook/paid-time-off) per 25 consecutive calendar days off.

## Growth and Development Benefit

GitLab supports team members who wish to continue their education and growth within their professional career. GitLab team-members are eligible for a reimbursement of up to $10,000 USD per calendar year (January 1st - December 31st) depending on tenure, performance, company need for the learned skill, and available budget. A course is considered to be included in the calendar year in which the course is paid/reimbursed (which should also be the same calendar year in which the course ends). 

Budget estimations for the Growth & Development benefit are based on utilization rates from past years, adjusted for the size of the company. The budget is managed on a company-wide scale, not division-specific. For example, approving reimbursement for a team member in Engineering does not "take away" budget from other engineers. Eligibility is managed on an individual basis. Eligibility for this benefit is part of each GitLab team member's Total Rewards package.

### Growth and Development Benefit Eligibility 

Team members who are full-time and have been employed for three months can participate in this benefit. If you are a part-time GitLab team member, you may still be eligible to participate in this benefit but your reimbursement might be reduced. These situations will be discussed on a case-by-case basis.

Examples of requests that may be approved:
* A Backend Engineer who is not on a performance improvement plan seeking a bachelor's degree in Computer Science.
* A People Operations Generalist seeking a master's in Human Resources with a concentration in International Employee Relations.
* A Security Analyst seeking to take courses to gain a Cybersecurity certificate through an accredited college or university.
* A Product Marketing Manager seeking to take courses to become a Certified Brand Manager from The Association of International Product Marketing and Management.
* A Technical Account Manager seeking to obtain a Cloud Certification such as CompTIA Cloud, AWS Certified Solutions Architect, MicroSoft MCSA/MCSE, Cisco CCNA/CCNP.
* Learning how to code for all team members (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)).

Examples of requests that may be denied:
* A Marketing Manager seeking a master's in Human Resources, but has no intention of applying to a role in people ops.
* A Frontend Engineer seeking a master's in computer science who is on a performance improvement plan or having discussions around underperformance.
* If the tuition reimbursement budget has been reached, then your request may be denied, even if you meet eligibility requirements.

### How to Apply For Growth and Development Benefits

**For Growth & Development Benefits that cost under $1,000 USD**
We encourage you to seek verbal approval from your manager before you sign up to anything. If the cost is under $1,000 USD, please fill out the [Growth & Development Benefit Form (Under $1,000)](https://forms.gle/ZLH2KtEqxBtW1Bja6) and submit your expenses via [Expensify](https://about.gitlab.com/handbook/finance/expenses/#expense-reimbursement). 

**For Growth & Development Benefits that cost over $1,000 USD**
Once your manager has verbally approved your plan, please fill out the [Growth & Development Benefit Form (Over $1,000 USD)](https://docs.google.com/forms/d/e/1FAIpQLScoql6Yr-r8j_XjUjJNnJRzgf2rOsOE-gXpydn6r1INLqEfKw/viewform) at least 30 days before the commencement date. Total Rewards will stage a Growth & Development Agreement for you and obtain signatures from the approvers per the table below. If the education provider does not allow for tuition/payment deferment and this would cause financial strain, you can request to receive 50% of the reimbursement up front and the other 50% upon successful completion of the course/program (this option is available for reimbursements above $1,000 USD only as it is processed via payroll).

Reimbursements above $1,000 USD will be paid via payroll in the following pay cycle after you have completed the program and meet any extra eligibility requirements stated in the table below. For Your 4th Trimester Coaching, GitLab will pay the provider directly.

If you voluntarily terminate employment with GitLab prior to completing twelve consecutive months of active employment, expenses of $1,000 USD and over will need to be refunded to GitLab. If you opted to receive 50% of the reimbursement up front but the total amount on your form is at least $1,000 USD, the amount will still need to be refunded back to GitLab.

### Types of Growth and Development Reimbursements

| Category | Description | Eligibility for Reimbursement | Approver (above $1,000 USD) |
| -------- | ----------- | ----------------------------- | --------------------------- |
| Academic Study | GitLab supports team members who wish to continue their education and growth within their professional career. <br> The benefit will cover only the tuition and enrollment related fees. <br> Additional fees related to parking, books, supplies, technology, or administrative charges are not covered as part of the program. Tuition will be validated by receipt showing proof of payment. | - The course must be from a credentialed college or university and must be relevant to your role’s goals and development. <br> - A [Growth & Development form](https://docs.google.com/forms/d/e/1FAIpQLScoql6Yr-r8j_XjUjJNnJRzgf2rOsOE-gXpydn6r1INLqEfKw/viewform) submission will be needed for each semester, however, the relevance of the classes taken for each semester will be subject to the approval of the manager and e-group leader. <br> - The grade must be equivalent to a “B”. A final grade report or transcript is required to receive reimbursement. | Manager, People Business Partner, E-Group Member |
| Your 4th Trimester Coaching | Your 4th Trimester is a 90-day coaching program with Broad Perspective Consulting. The purpose of the program is to help parents (regardless of gender) be as prepared as possible to be a working parent through coaching and development. The cost of this program is $2,300 USD. | - Team members returning from Parental Leave. <br> - The provider will invoice GitLab directly. | Manager and People Business Partner |
| Professional Coaching | Professional coaching for managers and leaders to support their development in their area of interest.  We encourage team members to utilize GitLab's internal resources but understand that some team members may desire to have additional external coaching resources to develop their leadership skills. Individual contributors are encouraged to receive coaching and mentorship from their managers and our internal resources. | - Managers/Grade 8 and above. <br> - Invoice from the coach is required to receive reimbursement. | Manager, People Business Partner, E-Group Member |
| English Language Courses | As GitLab expands globally, we want to support our team members where English may not be their first language. Managers are encouraged to recommend this benefit to team members whose engagement as a part of GitLab and/or performance may be improved by further developing their English language skills. Examples of English Language Courses: <br> - [Coursera](https://www.coursera.org/browse/language-learning/learning-english) offers a wide variety of online English courses in partnership with various reputable colleges and universities. <br> Their courses range from improving your English language skills to more specialized courses that focus on English for STEM or career development. <br> - [LinkedIn Learning](https://www.linkedin.com/learning/search?keywords=english) has many courses for English. <br> - [Writing in Plain English](https://www.linkedin.com/learning/writing-in-plain-english) seems particularly well suited for improving async communication. Language learning Apps and sites can be effective tools. <br> - [Duolingo](https://www.duolingo.com/) offers gamified way to learn English, plus other languages. <br> - [Memrise](https://www.memrise.com/) is helpful for vocabulary building. <br> - Use [italki](https://www.italki.com/teachers/english) to find a English tutor. 1:1 instruction is often necessary for continued improvement, especially once a person has reached basic proficiency. <br> - English Language Courses offered in-person or online by a local college or university or English language courses offered online by a non-local college or university. | Courses offered in-person or online by a credential college or university or an English language school are eligible for reimbursement. | N/A as a course amount isn’t likely to exceed $1,000 USD. If this does exceed $1,000 USD, the academic study criteria would apply. |
| Professional Development/Certifications/Licenses | Any certifications/licenses that you and your manager think will be relevant to your development. | Certifications and licenses related to your role. For reimbursements above $1,000 USD, a final grade report or satisfactory certificate of completion will be required to receive reimbursement. | Manager, People Business Partner, E-Group Member |
| Workshops and Conferences | Work-related conferences including travel, lodging and meals. We encourage people to be speakers in conferences. More information for people interested in speaking can be found on our [Corporate Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/#speakers) page. We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environment. | - Before scheduling any travel or time off to attend a conference, please discuss your request with your manager. <br> - The manager will approve the request if the conference is work-related and the timing doesn't interfere with GitLab deliverables. After manager approval, the team member can schedule travel and will be reimbursed for related expenses. | Manager, People Business Partner, E-Group Member | 
| Self-Service Learning (LinkedIn Learning, Coursera, Others) | The company will pay for all courses related to learning how to code (for example Learning Rails on Codecademy), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel. | - The course must be related to your role.<br> - A final grade report or satisfactory certificate of completion are required to receive reimbursements over $1,000 USD. | Manager, People Business Partner, E-Group Member | 

#### Tax Implications for Tuition Reimbursement by Country

In some countries, the Growth and Development Benefit may be considered as taxable income and can be (partially) exempted from personal income taxes or subject to employer withholding taxes.

For example, in the United States 2021 tax year, if GitLab pays over [$5,250 for educational benefits for you during the year, you must generally pay tax on the amount over $5,250](https://www.irs.gov/newsroom/tax-benefits-for-education-information-center).

Please contact [Payroll](mailto:payroll@gitlab.com) or [People Ops](peopleops@gitlab.com) for any tax-related questions in your country.


### Administration of Growth and Development Reimbursements (Over $1,000)

1. A new form submission will notify the team that a new entry has been submitted and autofill a Growth and Development Benefit Agreement.
1. Check the team member's eligibility and make sure the cost is within the $10,000 USD annual limit.
1. Download the team member's Growth and Development Benefit Agreement from the "Populated Agreements" folder and stage the document in HelloSign.
1. Fill out columns L-R on the Growth and Development Benefit Log sheet.
   * For 4th Trimester expenses, tag Total Rewards on the sheet. 
1. Once all parties have signed the agreement, send an e-mail to the team member to notify them that the agreement has been signed and to send their transcript, certificate of completion or invoice (refer to the [reimbursement category table](/handbook/total-rewards/benefits/general-and-entity-benefits/#types-of-growth-and-development-reimbursements)) once they are ready to receive the reimbursement.
1. Save the signed agreement in the team member's BambooHR "Benefits and ISO" folder.
1. Upon completion, update columns L-R on the sheet. Tag Total Rewards on the sheet to notify Payroll via the appropriate Payroll Changes files. 

### Administration of Your 4th Trimester 

1. Once a completed form has been sent to Total Rewards, the team will process for final approval. 
1. When all signatures are complete, Total Rewards will file the form in BambooHR. 
1. If the team member lives in a country where this benefit is considered taxable income (currently only in Australia and some cases in Belgium), notify payroll.
1. Total Rewards will send an introductory email to Your 4th Trimester to the team member and Barbara Palmer `barbara@your4thtrimester.com` outlining the expected leave date, expected return to work date and date of birthing event. Barbara will conduct the intake with the team member and communicate directly for any coaching throughout the program. 

**Monthly Reporting:**
GitLab will provide Your 4th Trimester with a report monthly of any team members with an upcoming parental leave: expected leave date, expected return to work date, date of birthing event, country, whether the person has already opted in, and maternity or paternity leave. The purpose of this monthly report is to ensure Your 4th Trimester can plan accordingly to the volume of coaching for GitLab team members. 

**Billing:**
Your 4th Trimester will bill GitLab for this benefit quarterly with 50% at intake and 50% at the completion of the coaching program per participant. If there are more than 5 participants in a quarter, there will be a discount on the bill. If there are any tax implications for the GitLab team member, Total Rewards will work with payroll to ensure this is reported compliantly. 

## GitLab Contribute
Every nine months or so GitLab team members gather at an exciting new location to [stay connected](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call [GitLab Contribute](/company/culture/contribute). It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also bought into the company vision. There are fun activities planned by our GitLab Contribute Experts, work time, and presentations from different Departments to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous).

## Business Travel Accident Policy

[This policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing) provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims, please reference the [claims guide (internal only)](https://drive.google.com/file/d/1vmLjhebsf81N8oSxqlCihYg5q1WT8Efw/view?usp=sharing).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
   * For more detailed information on this benefit, please reference the [policy document](https://drive.google.com/file/d/1ktx_mhlEYyQoLrQJ7DhIcibQhrlnB-lb/view?usp=sharing).
   * If you need a confirmation of coverage letter, please reference the [visa letter generation document (internal only)](https://drive.google.com/file/d/1oesZnp-fVWWCakVejB7nTV39lntMFnSd/view?usp=sharing).
   * For any additional questions, please contact the Total Rewards Analyst.

## Immigration

GitLab offers benefits in relation to [obtaining visas and work permits](/handbook/people-group/visas/) for eligible team members.

## Employee Assistance Program

GitLab offers an Employee Assistance Program to all team members via [Modern Health](/handbook/total-rewards/benefits/modern-health).

## Incentives

The following incentives are available for GitLab team members:
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

## All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._


### Part-time contracts

As part of our Diversity, Inclusion & Belonging  value, we support [Family and friends first](/handbook/values/#family-and-friends-first-work-second) approach. This is one of the many reasons we offer part-time contracts in some teams.

We are growing fast and unfortunately, not all teams are able to hire part-time team members yet. There are certain positions where we can only hire full-time team members.

Candidates can ask for part-time contract during the interview process. Even when a team they are interviewing for can't accept part-time team members, there might be other teams looking for the same expertise that might do so. If you are a current team member and would like to switch to a part-time contract, talk to your manager first.

## Meal Train

In order to foster a sense of community and support fellow GitLab team members, if a team member is in need or experiencing a life disruption, they have the option of having a Meal Train started for them. A Meal Train is when a community comes together to support someone experiencing a difficult or disrupting event in their life who would benefit from having a meal provided to them during this time. 

Examples of when someone may request a meal train (this list isn't exhaustive):
* Arrival of a new baby
* Injuries/Surgeries
* Military deployment
* Extended illnesses
* Condolences

### Instructions for starting a Meal Train

1. You may create a Meal Train for yourself or for a fellow team member. If you are not the recipient of the Meal Train, please receive approval from the team member first and forward this to the Total Rewards team. We highly encourage managers to reach out to their team member to see if they would like a Meal Train if the manager informs them of a life disruption.
1. When creating a Meal Train, please create this in GitLab using the [Meal Train issue template](https://gitlab.com/gitlab-com/people-group/total-rewards/-/blob/master/.gitlab/issue_templates/meal_train.md).
    * Steps for completing the form are included in the issue template.
    * If you are completing this for someone else, please reach out to them to ensure the form is being completed accurately.
    * The Total Rewards team is also happy to create the issue on your behalf if you send the form details to total-rewards@ domain. 
1. The issue template has 5 pre-populated entries to add a date to, but the Meal Train can be longer or shorter than this. Please add as many dates that are needed and if at the end, you feel more are needed, please add the additional dates and notify the Total Rewards Analyst assigned to the issue that you have added dates so they can help gather support.

### Instructions for participating in a Meal Train

1. All Meal Trains are posted by a Total Rewards Analyst in the #whats-happening-at-gitlab and #total-rewards slack channels. You can also check the [Total Rewards issue tracker](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues) where all Meal Trains will be designated as such. 
1. If there is a Meal Train occuring, please read through the entire issue. If you are interested in participating, please edit the issue and add your name to the date you would like to contribute to. There is also a field to add the meal you will be providing or the meal delivery service you intend to gift. More instructions for how to proceed as a team member will be located in the issue.
    * Please note, it is typically only possible to gift a gift card from within the same country. The Total Rewards team is researching possible ways to participate from a different country than the recipient, but until we have a solution, we don't advise participating in this from a different country and can not help facilitate this.
1. Ensure you can follow through on the date you sign up for or please help coordinate a replacement as soon as you know you can no longer participate. 
1. Participation is voluntary and is **NOT** reimbursable by GitLab. Any expense incurred would be your responsibility. 

If you have any questions, please reach out to the Total Rewards team.

## Parental Leave

GitLab offers anyone (regardless of gender) who has been at GitLab for six months and completed a [probationary period](/handbook/contracts/#probation-period) (if applicable) up to **16 weeks of 100% paid time off** during the first year of parenthood. This includes anyone who becomes a parent through childbirth or adoption. The 16-week balance is per birth or adoption event. If you live in a country where a statutory parental leave benefit is available, you will be required to claim statutory parental leave pay (if you are eligible) and GitLab will supplement any gaps. 

We encourage parents to take the time they need. GitLab team-members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. For many reasons, a team member may require more time off for parental leave. Many GitLab members are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 16 weeks just isn't enough. Any GitLab team member can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a six months and completed your probationary period your parental leave is fully paid. If you've been at GitLab for less than a six months it depends on your jurisdiction. If applicable, commissions are paid while on parental leave based on the prior twelve months of performance with a cap at 100% of plan. For example, if in the twelve months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only. The rate of commissions paid during your leave will be confirmed by the sales commissions team and communicated to the team member through the total rewards team prior to your leave start date. If your country of employment has specific laws regarding the payout of commissions when on leave, those would supersede our policy.

Additionally, effective Q1 FY22, commissioned roles are eligible to receive quota relief during their parental leave if the team member has been with GitLab for eight months or longer. The team member will recieve credit and commissions as specified in their existing Participant Schedule that occur ***prior*** to the start of the leave.  After 60 continuous days of parental leave, quota relief will be applied to the team member on leave for the total duration of the leave up to a total of 4 months. The team member would recieve an amended Participant Schedule upon return from leave. That amended participant schedule will include a prorated OTI based off the number of days that a team member is in seat. Please see a [reference slide here](https://docs.google.com/presentation/d/1pWM8UhJGL7G8w_QzLKM1q2Rqql4OKm7vLD9HSCKfT9g/edit#slide=id.p) for an example of how the quota relief process and calculation works. For more information on quota relief and commissions on leave, please refer to the terms in the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/#sales-compensation-plan).

You are entitled to and need to comply with your local regulations. They override our policy.

Some countries require extra paperwork or have specific leave requirements, which are subject to change as legal requirements change. Please take a look at your [country's leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits).

### How to Initiate Your Parental Leave

Some teams require more time to put a plan of action in place so we recommend communicating your plan to your manager at least 3 months before your leave starts. In the meantime, familiarize yourself with the steps below and specific leave requirements in your [country](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#general-and-entity-benefits) (if any). 

**To initiate your parental leave, submit your time off by selecting the Parental Leave category in PTO by Roots at least 30 days before your leave starts.** We understand that parental leave dates may change. You can edit your PTO by Roots at a later time if you need to adjust the dates of your parental leave. It's important that you submit a tentative date at least 30 days in advance. Your manager and the Total Rewards team will get notified after you submit your leave. Total Rewards will send you an e-mail within 48 hours confirming that they've been notified of your Parental Leave dates.

Please note, even though we have a "no ask, must tell" Parental Leave Policy, some countries require extra paperwork or notifications to a PEO so it's important that the Total Rewards team is aware of your leave **at least 30 days before your leave starts.**

### Planning Your Parental Leave Dates

Your 16 weeks of parental leave starts on the first day that you take off. This day can be in advance of the day that the baby arrives. You don't have to take your parental leave in one continuous period, we encourage you to plan and arrange your Parental Leave in a way that suits you and your family's needs. You may split your Parental Leave dates as you see fit, so long as it is within the 12 months of the birth or adoption event.

If you don't meet the initial requirements for paid leave, GitLab payroll coverage begins once you meet the requirements. Until then, you will not receive pay.  If for example, you are someone who qualifies after 6 months at Gitlab and then goes on leave at the start of your 120th day at GitLab, you would not receive payment from GitLab for the first 60 days. You would receive payment from GitLab for up to 60 additional days taken within a year from the birth event.

You can change the dates of your parental leave via PTO by Roots. Total Rewards will receive a notification every time you edit your Parental Leave dates. Make sure your leave is under the Parental Leave category, otherwise Total Rewards won't get a notification.
Please note, if you are planning to change or extend your Parental Leave by using a different type of leave such as PTO, unpaid leave or any local statutory leave, please send an e-mail to Total Rewards.

### Taking PTO After Taking Parental Leave

If you need to take more time off after taking 16 weeks of parental leave, you can utilize our [Paid Time Off policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off). You'll need to return for at least 7 calendar days before taking any further time off since the full 16 weeks of parental leave will have been used. 

Please e-mail Total Rewards if you'd like to make this request and submit your PTO dates in [PTO by Roots](https://about.gitlab.com/handbook/paid-time-off/#pto-by-roots).

### Returning from Parental Leave

To [alleviate the stress](/handbook/paid-time-off/#returning-to-work-after-parental-leave) associated with returning to work after parental leave, GitLab supports team members coming back at [50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Parents at GitLab who are reentering work following parental leave are encouraged to reach out to team members who self-designate as a [Parental Leave Reentry Buddy](/handbook/total-rewards/benefits/parental-leave-toolkit/).

Managers of soon to be parents should check out this [Parental Leave Manager Tool Kit](/handbook/total-rewards/benefits/parental-leave-toolkit/#manager-tool-kit) for best practices in supporting your team members as they prepare for and return from Parental Leave.

If you're interested in learning about how other GitLab team members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.


### Process for Total Rewards Analysts
  * PTO by Roots will notify Total Rewards of any upcoming parental leave.
  * Log and monitor upcoming parental leave in the "Parental Leave Log" Google sheet on the drive.
  * Notify the team member that the parental leave request was received by sending the confirmation e-mail template (only send this e-mail if the team member is starting their parental leave. Team members who are taking their leave intermittently or changing the dates of their original leave request do not need to receive the confirmation e-mail again).
  * Notify payroll - add the start date and end date on the payroll changes file ("Payroll Changes" sheet for team members in the US, "Canada Payroll Changes" sheet for team members in Canada and "Non-US Payroll Changes" sheet for everyone else). Ensure the entry is under the appropriate pay cycle date.
  * PTO by Roots will automatically update the employment status in BambooHR. Note: If the team member changes the date of their parental leave or cancels their request, the BambooHR employment status will need to be updated manually. 
  * Check local statutory requirements and if applicable, arrange any paperwork and liaise with appropriate parties (such as the PEO and payroll team) to initiate the parental leave.
  * 1 day before the End of Parental Leave date, BambooHR will send a notification to Total Rewards. Confirm the team member's return by sending the return to work e-mail to the team member and manager. If the team member or manager doesn't reply at least 1 business day after the End of Parental Leave Date, follow up again or ping the manager to confirm the team member has returned from leave.
  * If there are no changes to the End of Parental Leave date, update the team member's BambooHR status to "Active" and mark the status as "Completed" in the Parental Leave Log sheet.
  * If the dates in PTO by Roots change, update the payroll changes file and the BambooHR status accordingly.
