---
layout: handbook-page-toc
title: Alliance Partners
description: "Support specific information for alliance partners"
---

Also known as Technology Partners, Alliance Partners are looking to integrate
with our solution. Through product integrations, GitLab helps developers
compile all their work into one tool that can be accessed anywhere. We work
closely through partnerships to provide developers with a single DevOps
experience.

## Contacting Support

Alliance Partners contact us via the
[support portal](https://support.gitlab.com). To help them route properly, they
use
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001172559).

This should open the ticket in the Zendesk Alliance Partner form in zendesk.

**Note**: Pay close attention to any organization notes the Alliance Partner
might have on their account. These often provide critical information about how
to provide the very best support possible!

## File uploads

When Alliance Partners needs to send support files, we have 3 current methods
available to accomodate this:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)
* A specialized connection to an s3 bucket

To work with the specialized connection to an s3 bucket, support will have an
application available to them to list and download the files as needed.

## Escalations

Alliance Partners work closer with GitLab Support. To accomodate this, GitLab
Support provides all Alliance Partners with an escalation form. When filled
out, this form will get a corresponding GitLab Support Manager involved to help
escalate a ticket the Alliance Partners have opened.
