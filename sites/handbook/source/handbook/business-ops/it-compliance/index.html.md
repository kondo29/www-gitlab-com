---
layout: handbook-page-toc
title: "IT Compliance"
description: "The IT Audit and Compliance function at GitLab is here to ensure as a company we are ready to pass a SOX Audit for our IT General Controls (ITGC)."
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach out to us? 

<div class="flex-row" markdown="0">
  <div>
    <h5>Issue Trackers</h5>
    <a href="https://gitlab.com/gitlab-com/business-ops/it-compliance/it-automation/change-log-automation/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">System Change Log Automation</a>
    <a href="https://gitlab.com/gitlab-com/business-ops/it-compliance/it-automation/user-export-automation/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">User Export Automation</a>
    <a href="https://gitlab.com/gitlab-com/business-ops/it-compliance/it-access-review/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Compliance Access Review</a>
    <a href="https://gitlab.com/gitlab-com/business-ops/it-compliance/it-compliance-issue-tracker/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">IT Compliance Issue Tracker</a>
  </div>
 </div>

IT Compliance works collaboratively with multiple functional teams throughout the GitLab organization. We partner with our Security Compliance and Legal teams to identify and manage privacy, data protection risks, and compliance requirements to help meet stakeholder expectations. We also partner with Management, Business Teams, and our Data Team to implement solutions. 

Our work can be tracked in the [IT Compliance GitLab Group](https://gitlab.com/gitlab-com/business-ops/it-compliance). 

**Note** The Compliance Access Review Project is where we are logging and storing the main issue IT Compliance uses to complete User Access Reviews. The actual User Access Review issues are still being tracked in our [Access Request Project](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues).

## Who We Are

The IT Audit and Compliance function at GitLab is here to ensure as a company we are ready to pass a SOX Audit for our IT General Controls (ITGC). IT Audit and compliance builds the processes that allow us to stay compliant over time. We are specialized around Business Technology and that is our area of focus. Our work rolls up to the overall Security portfolio of Audit and Compliance. 

**Vision**

1. To identify and secure applications that are deemed to fall under SOX Audit
1. Ensure that only current employees have access to the applications and the appropriate actions. 
1. Manage all changes to SOX compliant systems to ensure their auditability and compliance with SOX level change management.
1. Constantly iterate to simplify and ensure processes are efficient and automated as much as possible.  Goal is to weave these processes into the fabric of work so they are not noticed. 
1. IT Audit and Compliance - Ensuring that all customer/business data is secure and can pass key audits for attestations and compliance with SOX, SOC, etc. 

## How we work

Our [IT Compliance Board](https://gitlab.com/groups/gitlab-com/-/boards/1802558?label_name[]=IT%20Compliance) board is where some of our work can be tracked. If you need help with anything or have any questions, you can add our label `IT Compliance` or tag `@gitlab-com/business-ops/it-compliance` in an issue. You can also find us hanging around in the `# business-operations` slack channel. 

## What we do

#### IT General Controls 

**Most Common:**

The most common ITGCs:

* Logical access controls over infrastructure, applications, and data.
* System development life cycle controls.
* Program change management controls.
* Data center physical security controls.
* System and data backup and recovery controls.
* Computer operation controls.
 
**GitLab’s IT Audit Function will focus on the following for the next 3 months:**
* Logical access controls over infrastructure, applications, and data.
* System development life cycle controls.
* Program change management controls.
* System and data backup and recovery controls.

#### [Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan)

IT Compliance works closely with our Security Compliance team to ensure that GitLab's Business Continuity Plan is up to date. 
