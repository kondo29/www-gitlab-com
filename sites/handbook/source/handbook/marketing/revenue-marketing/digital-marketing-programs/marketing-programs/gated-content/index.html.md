---
layout: markdown_page
title: "Gated Content"
---

## Overview
This page focuses on gated content as an offer for our marketing channels and integrated campaigns, including in-house created content, analyst relations content, and downloadable competitive content. If you have any questions about content planning, please visit the [Global Content handbook page](https://about.gitlab.com/handbook/marketing/growth-marketing/content/#what-does-the-global-content-team-do), and with questions about the process of tracking and landing page creation below, please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

#### Types of Content Programs
* Gated Content: We have the piece of content offered on our website via landing page.
    * [GitLab Created Content](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#internal-content-created-by-the-gitlab-team): We created the content in house
    * [Analyst Content](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#analyst-content-delivered-by-analysts): We have bought the rights to use the content from an analyst such as Gartner, Forrester, etc.
    * [On-Demand Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
* [Content Syndication](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication): We have promoted our content through a third-party, but do not direct back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

#### Gating Criteria

| Gated                                                | Not Gated                                                                          |
|------------------------------------------------------|------------------------------------------------------------------------------------|
| Whitepapers                                          | Blogs                                                                              |
| eBooks                                               | Infographics                                                                       |
| Reports                                              | Technical Training Resources                                                       |
| On-Demand Webcasts                                   | Customer Testimonials                                                              |
| Comparison PDFs                                      | Case Studies                                                                       |

> Note: if a new type of gated content needs to be introduced, and it is expected that it will be a regularly created piece of content (for example, as we begin to roll out Comparison PDFs), please post in [#marketing_programs slack channel(https://gitlab.slack.com/archives/CCWUCP4MS) to determine if steps need to be taken with Marketing Ops and Sales Ops for automation and reporting.

### Internal Content (created by the GitLab team)

Alignment to Campaigns: All gated content should be aligned to a content pillar (owned by Content Marketing) and a primary integrated campaign (owned by Marketing Campaigns). If no active or planned campaign is aligned, a campaign brief (including audience data) must be created for use by Digital Marketing Campaigns. _There should be a clear reason why we are moving forward with content that doesn't align to an integrated campaign._

Reporting: when someone submits a form on the gated content landing page, an online bizibe touchpoint is created containing rich utm parameters. The campaign landing page often uses different language aligned to the integrated campaign messaging.

#### Organizing content pillar epics and issues

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, with launch date determined and DRI assigned

1. **Content Pillar Epic:** `Content DRI` creates content pillar epic
1. **Content Asset Epics:** `Content DRI` creates content asset epics (using code below) and associates to pillar epic
1. **Related Issues:** `Content DRI` creates the issues as designated in the epic code, and associates to the content asset epic

[View the workback timeline calculator here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

```
<!-- NAME EPIC: *[type] Name of Asset* (ex. [eBook] A beginner's guide to GitOps) -->
<!-- START DATE = date epic opened, DUE DATE = launch date -->
<!-- DELETE THIS LINE AND ABOVE ONCE EPIC EDITED -->

## Launch date: 

## [Live landing page link]() - `to be added when live`

#### :key: Key Details
* **Content Marketing:**  
* **Marketing Campaigns:** 
* **Product Marketing:** 
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary Persona:** 
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Use Case:** 
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Language:** 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()

## :memo: Documents to Reference
* [ ] [asset copy draft]() - `doc to be added by Content Marketing`
* [ ] [landing page copy]() - `doc to be added by Content Marketing` ([use template here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679))
* [ ] [creative requirements]() - `Content DRI to link to new tab on relevant use case creative googledoc` - [handbook for more info]()
* [ ] [creative final]() - `Design DRI to link to repo in GitLab`

### :books:  Issues to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

**Required Issues:**
* [ ] [Asset Copy Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issuable_template=content-resource-request) - Content
* [ ] [Asset Design Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issuable_template=design-request-content-resource) - Digital Design
* [ ] [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issuable_template=landing-page-copy) - Content
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) - Content
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-mkto-landing-page) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns
* [ ] AR to add issue template for creating /analysts/ page (if desired)
* [ ] [Asset Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM) - `need to break these tasks out` MPM to organize for now

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) - Digital Marketing
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR

</details>

/label ~"Content Marketing" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::plan"
```


### `Analyst Content` (delivered by analysts)

When **Analyst Relations** determines that they will be purchasing analyst content to be gated, they are responsible for creating the epics and associated issues to request work of all relevant teams (outlined below to try to make it efficient, comprehensive, and repeatable!).

If the analyst content is thought leadership (i.e. a whitepaper or non-competitor comparison report), it should be planned in advance of purchase with `at least a 30 business day time to launch date`. This allows time to plan activation into existing and future integrated campaigns and content pillars.

[View the workback timeline calculator here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

#### Organizing analyst content epics and issues

🏷️ **Label statuses:**
* **status:plan**: analyst content has not yet been purchased, not yet approved and no DRI assigned
* **status:wip**: analyst content is approved for purchase, delivery date and launch date determined, DRI assigned

1. **Analyst Relations Asset Epic:** `Analyst Relations DRI` creates epic (using code below)
1. **Related Issues:** `Analyst Relations DRI` creates the issues as designated in the epic code, and associates to the analyst relations asset epic

*Please note that if details and landing page copy are not provided, the MPM is blocked and will not be able to gate the resource.*

```
<!-- NAME EPIC: *[type] Name Exactly as Appears On Report* (ex. [report] 2020 Gartner MQ for Application Security Testing (AST)) -->
<!-- START DATE = date epic opened, DUE DATE = launch date -->
<!-- DELETE THIS LINE AND ABOVE ONCE EPIC CREATED -->

## Launch date: 

## Live Pages: [/analyst/ commentary page]() and [download page]() - `to be added when live`

#### :key: Key Details
* **Analyst Relations:** 
* **Product Marketing:** 
* **Marketing Campaigns:** 
* **Content Marketing:** 
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary Persona:** 
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Use Case:** 
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Validity / Expiration Date (if relevant):** (start date - end date)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()

## :traffic_light: Green light?

:white_check_mark: *If the decision is made to purchase the report, AR opens the below documents and issues to request work from the relevant teams to execute toward launch.**

:no_entry: *If the decision is made **not** to purchase the report, AR closes out the epic.*

## :memo: Documents to Reference
* [ ] [content draft]() - `link to temporary unlicensed version when available`
* [ ] [landing page copy]() - `doc to be added by Analyst Relations` ([use template here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679))
* [ ] [promotion creative requirements]() - `Design DRI to link to new tab on relevant use case creative googledoc` - [handbook for more info]()
* [ ] [promotion creative final]() - `Design DRI to link to repo in GitLab`

### :books:  Issues to Create

**Required Issues:**
* [ ] [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-Landing-Page-Copy) - AR
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) - Campaigns
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-mkto-landing-page) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns
* [ ] AR to add issue template for creating /analysts/ page (if desired)
* [ ] [Asset Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM) - `need to break these tasks out` MPM to organize for now

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) - Digital Marketing
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR

</details>

cc @jgragnola 

/label ~"Gated Content" ~"Analyst Relations" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::plan"
```

### `Competitive Content` (delivered by competitive)

When **Competitive Intelligence** determines that they will be purchasing a comparison PDF to be gated, they are responsible for creating the epics and associated issues to request work of all relevant teams (outlined below to try to make it efficient, comprehensive, and repeatable!).

[View the workback timeline calculator here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

#### Organizing competitive content epics and issues

🏷️ **Label statuses:**
* **status:plan**: competitive content has not yet been purchased, not yet approved and no DRI assigned
* **status:wip**: competitive content is approved for purchase, delivery date and launch date determined, DRI assigned

1. **Competitive Asset Epic:** `Competitive DRI` creates epic (using code below)
1. **Related Issues:** `Competitive DRI` creates the issues as designated in the epic code, and associates to the competitive asset epic

*Please note that if details and landing page copy are not provided, the MPM is blocked and will not be able to gate the resource.*

```
<!-- NAME EPIC: *[type] Exact Name of Content* (ex. [comparison pdf] Bitbucket vs. GitLab) -->
<!-- START DATE = date epic opened, DUE DATE = launch date -->
<!-- DELETE THIS LINE AND ABOVE ONCE EPIC EDITED -->

## Launch date: 

## Live Pages: [overall comparison page]() and [comparison pdf download page]() - `to be added when live`

#### :key: Key Details
* **Competitive Intelligence:** 
* **Product Marketing:** 
* **Marketing Campaigns:** 
* **Content Marketing:** 
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary Persona:** 
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Use Case:** 
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Validity / Expiration Date (if relevant):** (start date - end date)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()

## :memo: Documents to Reference
* [ ] [content draft]() - `link to temporary unlicensed version when available`
* [ ] [promotion creative requirements]() - `Design DRI to link to new tab on relevant use case creative googledoc`
* [ ] [promotion creative final]() - `Design DRI to link to repo in GitLab`

### :books:  Issues to Create

**Required Issues:**
* [ ] [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-Landing-Page-Copy) - Competitive
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload)
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track)
* [ ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-mkto-landing-page)
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition)

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) - Digital
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR

</details>

cc @jgragnola 

/label ~"Gated Content" ~competition ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::plan"
```

## Where to upload final asset

To be completed by the content owner if Pathfactory access is granted. If no access, please open an issue in Campaigns team based on epic category above.

**Add to Pathfactory**
* Follow handbook instructions [here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#how-to-upload-content)

**Add to /downloads/ repository** (only available and recommended for assets under 2 MB size)
*The purpose of this step is to make it possible to flip the autoresponder if Pathfactory were to have an outtage, at which point, we would still have the PDF version available in Marketo for a quick turnaround.*

1. Save the pdf to your computer with naming convention `[asset-type]-short-name-asset`, ex: `ebook-agile-delivery-models`
1. Navigate to the (de-indexed) [`resource/download`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/sites/marketing/source/resources) directory
1. Where it says `www-gitlab-com / sites / marketing / source / resources / +`, click the plus drop down and select "Upload File"
1. Upload the file you've saved to your computer with the naming convention above
1. For commit message, add `Upload [Asset Type]: [Asset Name]`, check box for "create new merge request", name your merge request, and click "Upload file"
1. Add description to MR, complete the author checklist, assign to `@jgragnola` and click "Submit Merge Request"
1. In your Marketo program, for the `pdfVersion` My Token, add the naming convention above which will be available when the MR is merged. (the token should look like `https://about.gitlab.com/resources/downloads/add-file-name-here.pdf`)

## Marketo automation and landing page creation

[Watch the video tutorial >](https://www.youtube.com/watch?v=RrmDCZPh1nw)

:exclamation: Dependencies: delivery of final asset, completion of final landing page copy, and final asset added to pathfactory and placed in a track must be complete before setting up the Marketo program.

**The TL;DR of what you'll do:**
* Create Marketo program, tokens, and SFDC campaign sync
* Edit registration page and thank you page URLs
* Activate smart campaign(s)
* Update SFDC campaign
* Test live registration page and flows
* Add new content to the Resources page (separate issue)

#### Create Marketo program, tokens, and SFDC campaign sync
  * Clone the [Marketo Gated Content Template](https://app-ab13.marketo.com/#PG5111A1) and name new program using naming convention (YYYY_Type_AssetName, i.e. 2020_report_GarnterVOC_ARO)
  * Create SFDC program (Program Summary > `Salesforce campaign sync` > click "not set" and choose "Create New" from dropdown) - leave the name as auto-populates, and add the epic url to the description and "Save"
  * Update Marketo tokens (Program Summary > "My Tokens" tab)
    * `{{my.bullet1}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet2}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet3}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet4}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.contentDescription}}`	- 2-3 sentences with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679), this will show up in page previews on social and be used in Pathfactory description.
    * `{{my.contentDownloadURL}}` - skip updating in initial registration page setup (update during on-demand switch), Pathfactory link WITHOUT the `https://` NOR the email tracking part (`lb_email=`)
      * Example of correct link to include: `learn.gitlab.com/gartner-voc-aro/gartner-voc-aro` - the code in the Marketo template assets will create the URL `https://learn.gitlab.com/gartner-voc-aro/gartner-voc-aro?lb_email={{lead.email address}}&{{my.utm}}`
      * Note that both parts of this url include custom URL slugs which should be incorporated into all pathfactory links for simplicity of tracking paramaeters
    * `{{my.contentEpicURL}}` - no longer used in automation, but helpful for reference
    * `{{my.contentSubtitle}}` - content subtitle to display to viewer (throughout landing page, emails, etc.)
    * `{{my.contentTitle}}`	- content title to display to viewer (throughout landing page, emails, interesting moments, etc.), with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.contentType}}`	- content type to display to viewer (throughout landing page, emails, interesting moments, etc.)
    * `{{my.contentTypeSFDC}}` - pick from following list (critical to avoid Marketo > SFDC sync errors): whitepaper, report, video, eBook, general
    * `{{my.emailConfirmationButtonCopy}}`	- leave as `Download`  (but can be updated if needed)
    * `{{my.formButtonCopy}}`	- leave as `Download  now` (but can be updated if needed)
    * `{{my.formHeader}}`	- leave as `Free Instant Download:` (but can be updated if needed)
    * `{{my.formSubhead}}`	- form subhead (not currently used for gated content landing page to try to keep form shorter)
    * `{{my.heroImage}}` - image to display above landing page form ([options in Marketo here](https://app-ab13.marketo.com/#FI0A1ZN9784))
    * `{{my.introParagraph}}`	- intro paragraph to be used in landing page and nurture email, with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.mpm owner email address}}` - no longer used in automation, but helpful to know who to go to about setup
    * `{{my.or}}` - leave as `?` (but can be updated to `&` if during Pathfactory upload, the custom URL slugs were not applied to both the asset and the track). If this is not correctly applied and there are multiple `?` question marks in the URL, it will break. [WATCH THE VIDEO EXPLAINER](https://www.youtube.com/watch?v=VHgR33cNeJg)
    * `{{my.pdfVersion}}`	- this should be the GitLab repo link (for safety backup if Pathfactory were to go down)
    * `{{my.socialImage}}`	- image that would be presented in social, slack, etc. preview when the URL is shared, this image is provided by design/social, leave the default unless presented with webcast specific image.
    * `{{my.utm}}` - this should match the aligned campaign utm
    * `{{my.valueStatement}}` token with the short value statement on what the viewer gains from the webcast, this ties into the follow up emails and must meet the max/min requirements of the [character limit checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)

#### Edit registration page and thank you page URLs
  * Right click the landing page object > "URL Tools" > "Edit URL Settings"
  * Input new Registration Page URL (format: `resources-type-name-of-asset`, i.e. `resources-ebook-ci-best-practices`)
  * Input new Thank You Page URL (format: `resources-type-name-of-asset-thank-you`, i.e. `resources-ebook-ci-best-practices-thank-you`)
  * For both, leave `"Throw away" existing url` selected and click save

#### Edit "resulting page" from the form submit
  * The cloned program will automatically reference the Marketo program template Thank You Page
  * Right click the registration landign page > "Edit Draft"
  * On the right rail of the edit mode, under `Elements` right click on the `Form Custom` element and click "Edit"
  * Next to `Follow-up page`, clear out the automatic reference to the template, and start to type in your program name - the approved thank you page should show up. Choose your thank you page
  * Click "Swap" button at the bottom
  * At top left of page, click `Landing Page Actions` > "Approve and Close"

#### Activate smart campaign(s)
  * Click to `01 Downloaded Content` smart campaign
  * Smart List: *it's all set!* For your first few, feel free to check that it references the program landing page (it should do this automatically)
  * Flow: it's all set! For your first few, feel free to review the flows (but they are all using tokens, so it should be ready to go automatically)
  * Schedule tab: click "Activate" (note: the settings should be that "each person can run through the flow once every 7 days" - this is to avoid bots resubmitting repeatedly)

#### Update SFDC campaign
  * Navigate to [https://gitlab.my.salesforce.com/701?fcf=00B61000004NY3B&page=1&rolodexIndex=-1] campaigns in Salesforce
  * `Campaign Owner` should be the campaign creator
  * `Active` field should be checked
  * `Description ` must include the epic url, best practice to include the registration page URL
  * `Start Date` should be date of launch
  * `End Date` should be one year later
  * `Budgeted Cost` is required, if cost is $0 list `1` in the `Budgeted Cost` field - NOTE there needs to be at least a 1 value here for ROI calculations, otherwise, when you divide the pipeline by `0` you will always get `0` as the pipe2spend calculation. 
  * `Bizible Touchpoints Enabled` leave this blank (because this would be an online touchpoint)

#### Test live registration page and flows
  * Click to the landing page object and click "View Approved Page"
  * Final QA of all copy
  * Submit the form
  * Final QA that the form submit brings you to the thank you page
  * Final QA that the link on the thank you page sends to Pathfactory **with** the tracking for the email address (`&lb_email=[email submitted in form]`)
  * Check that you received the follow up email
  * Final QA of confirmation email copy
  * Final QA that the confirmation email link sends to Pathfactory with the tracking for the email address  (`&lb_email=[email submitted in form]`)

## Add new content to the Resources page
1. Begin a new MR from [the resources yml](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/data/resources.yml)
2. Use the code below to add a new entry with the relevant variables
3. Add commit message `Add [resource name] to Resources page`, rename your target branch, leave "start a new merge request with these changes" and click "Commit Changes"
5. Assign the merge request to yourself
6. When you've tested the MR in the review app and all looks correct (remember to test the filtering!), assign to `@jgragnola`
7. Comment to `@jgragnola` that the MR is ready to merge

**Code:**
```
- title: 'Add name of resource - shorten if necessary'
  url: 
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - 
    - 
  solutions:
    - 
    - 
  teaser: 'Add a teaser that relates to the contents of the resource'
```

**Example:**
```
- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
    - Security
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
```

**IMAGES to choose from (select one):**
*[Shortcuts to Images Folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/resources)
* `/images/resources/cloud-native.png`
* `/images/resources/code-review.png`
* `/images/resources/continuous-integration.png`
* `/images/resources/devops.png`
* `/images/resources/git.png`
* `/images/resources/gitlab.png`
* `/images/resources/security.png`
* `/images/resources/software-development.png`
* `/images/resources/resources-gitops.png`

**TOPICS to choose from (add all that apply):**
* Agile
* CD
* CI
* Cloud Native
* DevOps
* DevSecOps
* Git
* GitLab
* Public Sector
* Security
* Single Applicaton
* Software Development
* Toolchain

## How to retire analyst assets when they expire

* An [Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM) will be open for each analyst asset, and related to the overarching Epic upon gating (with due date for when the asset is set to expire)
* At times, we will extend the rights to an asset if it is heavily used by sales or performing well in later stage nurture. In that case the decisio nis indicated in the Expiration Issue, and the team is updated.