---
layout: handbook-page-toc
title: Core Infra Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

The Core Infra teams owns:

- infrastructure tooling like:
    - [Terraform](https://gitlab.com/gitlab-com/gitlab-com-infrastructure)
    - [Chef](/handbook/engineering/infrastructure/production/architecture/#chef-architecture)
- network ingress/egress
    - [Networking](/handbook/engineering/infrastructure/production/architecture/#internal-networking-scheme)
- [CDNs and DNS](/handbook/engineering/infrastructure/production/architecture/#dns--waf)
- [Secrets management](/handbook/engineering/infrastructure/production/architecture/#secrets-management)
- Helping with Fulfillment Stage assets like subscription and license management apps.

Need help? [How to get something on our backlog](#asks-of-the-infra-team-and-unplanned-work)

<%= partial "handbook/engineering/infrastructure/team/reliability/core-infra/_core_infra_team.html" %>

## Tenets

Our guiding principles center around implementing [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions) to infrastructure problems. We work to simplify interfaces and build robust workflows for other engineers within GitLab who utilize our platform to provide support for and deliver new features to the gitlab.com SaaS product. Over time this continues to expand to include additional/related applications, sites, and systems.

In practice, this means that we

1. Manage all infrastructure resources via Infrastructure as Code or auditing tools
    - GCP [by Terraform](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)
    - Cloudflare / CDN [audited by a tool](https://ops.gitlab.net/gitlab-com/gl-infra/cloudflare-audit-log)
1. Develop automated workflows ensuring consistency, repeatability, predictability, and reliability
1. [Use GitLab to deploy and manage our infrastructure](https://about.gitlab.com/handbook/values/#dogfooding)
1. [Maintain a user-centric focus](https://about.gitlab.com/handbook/values/#customer-results), and [enable self-service functionality](https://about.gitlab.com/handbook/values/#self-service-and-self-learning) whenever possible
1. Work with other SRE teams to ensure common principles are upheld and services are maintained according to our [production readiness standards](https://gitlab.com/gitlab-com/gl-infra/readiness)
1. [Focus on small changes](https://about.gitlab.com/handbook/values/#iteration) and [embrace experimentation](https://about.gitlab.com/handbook/values/#accepting-uncertainty)

## Vision

- Help with moving compute to GKE
    - Smaller Chef footprint
    - More Autoscaling
    - Secure and setup best practices for GKE cluster
- Create tools and processes for external teams to service requests themselves
- Move all Growth Stage apps to AutoDevops

## How we work:

#### Epics

We have 3 types of epics:

1. The [main team epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/326). This stores the roadmap/radar for the team.
There are then lists of epics of the next 2 types for what we are working on now, what is up next and what is on the radar.
1. Grouping or Backlog Epics (Themes). These epics are things on our radar/roadmap. They are meant to group together related areas of work.
They are not commitments to work. They are a way to help organize related areas into themes on our backlog.
1. Project Epics. These will usually sub-epics of the Grouping or Backlog Epics. We'll create these epics for work that is
["Up Next"](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/326#up-next-arrow_forward). These epics will be used to
scope the work of what is next and will be in progress.

#### Milestones

We use two types of milestones that can overlap:

1. Planned work milestones. These correspond to the Project Epics above. They give us a timebox in which we plan to do the work.
We can then watch burndowns for that work.
1. Unplanned milestones. These are short 2 week timeboxes in which we schedule external/unplanned asks of the team that were
not in Project Epics. They allow us to help set a budget for the incoming requests we do in a given timebox.

#### Workflow

- We use a [Kanban board](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1688496?milestone_title=%23started&label_name%5B%5D=team%3A%3ACore-Infra) to track work
- If you assign yourself to an issue, it goes in a Milestone, gets the team::CoreInfra label. It will then appear on the Board.
- We use a Geekbot standup daily to talk about what we are working on
- We do Core-Infra team retros monthly async via Geekbot put into the #sre_coreinfra channel

#### Asks of the infra team and unplanned work:

When external asks come in:

- they can be commented on, but get a Workflow-Infra::triage Label to start
- We make 2 week long milestones to have ["iterations" with the unplanned work](https://gitlab.com/groups/gitlab-com/gl-infra/-/milestones?utf8=%E2%9C%93&search_title=core+unplanned&state=&sort=).  This gives us a view to map out the work coming in and approximately when we can start that work.
- When added to the board, the should get labels `~AssistType::TYPE` and `~AssistingTeam::TEAM`, and be assigned to the next
 milestone with capacity to take on work with the `~workflow-infra::Ready` label.
- Emergent issues requiring a more urgent response should have a <a href="#issue-weights">weight estimate</a> added, get added to the current milestone, and labeled `~unplanned`. We reserve a <a name=team-capacity>fixed capacity</a> for emergent work each milestone, after which we begin to impact commitments to stakeholders and need to reschedule/reprioritize work.

### Planning

We conduct our planning and retrospectives asynchronously, using issues in the [sre-coreinfra/planning](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning) project to organize discussion.

#### Cadence

Planning occurs on a two-week cadence, aligned with our standard milestones.

During the first week of a milestone, a [planning issue](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning)  will be created for the team to review and discuss the _next_ upcoming milestone. This allows for time before the start of the next milestone to work on collecting additional data required to refine an issue, clarify details with requestors, or perform basic analyses of the relative size/scope of an issue _before_ starting the actual work.

In addition to resolving any questions or discussions raised on the planning issue for the next milestone, backlog issues will be triaged on an on-going basis, and scheduled into subsequent upcoming milestones. In general we maintain a horizon of 2-3 upcoming milestones, but that will fluctuate based on the flow of work and active projects.

#### Issue triage

Issue triage starts with reviewing rollover (issues in the current unplanned milestone that were not finished), incoming, unscheduled, and unprioritized work. Since rollover work is the most disruptive to being predictable in meeting our commitments, planning is conducted with the assumption that all work in a milestone will be completed, and we size our milestones accordingly.

Backlog issues will be reviewed in the following order

1. Incomplete issues from the current milestone are rolled over to an upcoming milestone, de-prioritized to the backlog, or canceled/closed
1. Open issues labeled `team::Core-Infra` and `workflow-infra::triage` move to `workflow-infra::ready`
1. New issues labeled `team::Reliability`
1. All triaged issues will be reviewd in aggregate and `core-infra::pX` priority labels added where necessary

##### Issue weights

During issue triage, we estimate the complexity of a task and time required for completion. Based on that assessment, we assign weight values according to the following matrix.

| Time / Complexity | *simple* | *average* | *complex* |
|:---:|---|---|---|
| *<1 day* | 1 | 2 | 3 |
| *1-2 days* | 5 | 8 | 13 |
| *2+ days* | 15 | 20 | 40 |

Point distribution/examples:
1. Basic user management tasks would have a weight of 1; this includes resetting a user's password, provisioning a new account, adding a user's SSH key to a chef data bag, scaling a node pool, updating firewall rules, etc.
1. Tasks of average complexity should be sized so that they can be completed within 1-1.5 days. This can account for issues that require information gathering, some final requirement validation, or data analysis before implementing a technical change. For example, reviewing a changelog for syntax changes before updating a terraform module for new syntax or new provider support.
1. Triage and planning issues themselves should account for ~5 points of effort. Team members will each need at least a couple hours each week to review issue weights for the upcoming milestone, and participate in discussions around both issue sizing, scheduling, and relative priority. Planning team members will also spend additional time on backlog grooming, initial issue triage, and milestone planning.
1. Most production changes will be ~5-13 points, depending on the amount of time needed for planning and testing. Taken independently, most production changes are planned, tested, and executed in less than a half day for each step, but require intense or uninterrupted focus for the duration.

Anything assessed at more than a 13 should be broken down further into smaller, more manageable tasks, or prompt serious discussion within the team about why that can't be accomplished. Large tasks can be scheduled into a milestone, but some additional effort should be scheduled to account for further planning and task breakdown.

Ideally milestones will be populated entirely with issues under 5 points, and for very large efforts we will spread the planning, breakdown, scheduling, and implementation of smaller, more manageable tasks across multiple milestones.

##### Team capacity

*Note that all the following is a work-in-progress, and subject to significant change as we refine our estimation and planning processes over time*

As a rule, we set an overall target weight of 90 for all issues within a milestone, and we reserve ~20% capacity for unplanned work that comes in after the start of a milestone.

These values are based on the following assumptions

1. On average, meetings consume ~20% of each IC's time
1. General async team-work (reviewing MRs, issue comments, processing email/todo/slack messages) comprises a further ~20%
1. Average efficiency losses for context switching and the we're-human-not-robots factor accounts for ~10%
1. Focused time on "direct" work coding, working at a terminal, on a console, etc. fills the remaining ~50%

Translating this to our <a href="#issue-weights">time/complexity matrix</a>, this means that each IC is estimated to have about 5-6 days for intense, focused work each milestone, or ~15 points on our weight scale. Extrapolating that out on the pessimistic side (because we'd rather under-promise and over-deliver as a general rule), we arrive at our overal target of $`number_{IC} * \min(capacity_{IC}) = targetWeight_{milestone}`$. For our current, specific case above, this is $`6 * (15) = 90`$

#### Planning discussion

Once the initial list of proposed issues for the upcoming milestone is completed, the team will perform a final review and discuss any additional context, missing details, pre-requisites, conflicts, etc. and decide on a final relative priority for work to be pulled.

In addition, we will further <a href="team-capacity">refine the overall target weight</a> for a specific milestone based on team availability. Scheduled time off, on-call shifts, and other priorities will be taken into account before starting each milestone. Some issues may need to be pushed back to future milestones if the initial list of issues under consideration exceeds the team capacity heading into the next milestone.

#### Retrospectives

At the conclusion of each milestone, a retrospective issue will be created in the [sre-coreinfra/planning](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning) project, for discussion and review. The text of the retrospective can be customized to ensure that certain aspects of the previous project/milestone are discussed, but in general will follow the format

1. What went well?
1. What did not go well?
1. What did you learn?
1. What still puzzles you?

### Labels

The Core-Infra team routinely uses the following set of labels:

1. The team label, `team::Core-Infra`.
1. Priority labels `Core-Infra::P1` through `Core-Infra::P4`.
1. Scoped `workflow-infra` labels.
1. Scoped `Service` labels.

The `team::Core-Infra` label is used in order to allow for easier filtering of
issues applicable to the team that have group level labels applied.

<a href="#priority-labels">The priority labels</a> allow us to track the issues correctly and raise/lower priority of work based on both external and internal factors.

This means that the highest priority is given to working on issues that improve
Gitlab.com SLO's either immediately and directly, or by unblocking other issues
to achieve the same.

#### Workflow labels

The Core-Infra team leverages scoped workflow labels to track different stages of work.
They show the progression of work for each issue and allow us to remove blockers or change
focus more easily.

The standard progression of workflow is from top to bottom in the table below:

| State Label | Description |
| ----------- | ----------- |
| `workflow-infra::Triage` | Problem is identified and effort is needed to determine the correct action or work required. |
| `workflow-infra::Proposal` | Proposal is created and put forward for discussion and review.<br>SRE looks for clarification and writes up a rough high-level execution plan if required. SRE highlights what they will check and along with soak/review time and developers can confirm.<br>If there are no further questions or blockers, the issue can be moved into "Ready". |
| `workflow-infra::Ready` | Proposal is complete and the issue is waiting to be picked up for work. |
| `workflow-infra::In Progress` | Issue is assigned and work has started.<br>While in progress, the issue should be updated to include steps for verification that will be followed at a later stage. |
| `workflow-infra::Under Review` | Issue has an MR in review. |
| `workflow-infra::Verify` | MR was merged and we are waiting to see the impact of the change to confirm that the initial problem is resolved. |
| `workflow-infra::Done` | Issue is updated with the latest graphs and measurements, this label is applied and issue can be closed. |

There are three other workflow labels of importance:

| State Label | Description |
| ----------- | ----------- |
| `workflow-infra::Cancelled` | Work in the issue is being abandoned due to external factors or decision to not resolve the issue. After applying this label, issue will be closed. |
| `workflow-infra::Stalled` | Work is not abandoned but other work has higher priority. After applying this label, team Engineering Manager is mentioned in the issue to either change the priority or find more help. |
| `workflow-infra::Blocked` | Work is blocked due external dependencies or other external factors. Where possible, a [blocking issue](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) should also be set. After applying this label, issue will be regularly triaged by the team until the label can be removed. |

#### Priority labels

The Core-Infra team uses priority labels as a means to indicate order under which work is next to be picked up. Priorities are roughly defined as:

| Priority level | Definition |
| -------------- | ---------- |
| `Core-Infra::P1` | Issue is blocking other team-members, or blocking other work. As soon as possible after completing ongoing task unless directly communicated otherwise. |
| `Core-Infra::P2` | Issue has a large impact, or will create additional work. |
| `Core-Infra::P3` | Issue should be completed once other urgent work is done. |
| `Core-Infra::P4` | **Default priority**. A nice-to-have improvement, non-blocking technical debt, or a discussion issue. |
