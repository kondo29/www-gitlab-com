---
layout: job_family_page
title: "VP of New Markets"
canonical_path: "/job-families/engineering/vp-of-new-markets/"
---

## Summary

The VP of New Markets runs the [New Markets Department](/handbook/engineering/new-markets/) within the Engineering Division. The team members in this department focus on long-term trends and features that are too early to measure via usage or revenue, but are critical to the long term growth and success of GitLab. The name "new markets" comes from one of our [three product investment types](/handbook/product/investment/#investment-types). Long term, the budget for this area will be ~10% of R&D. This position reports to our [Executive Vice President of Engineering](/handbook/engineering/readmes/eric-johnson/).

## Responsibilities

* Recruit excellent senior individual contributors to act as [Single-engineering Groups (SEGs)](/company/team/structure/#single-engineer-groups)
* Effectively wield technical influence within a larger company and codebase
* Provide [lightweight project management](/handbook/engineering/#engineering-demo-process) to the Single-Engineer Groups under their purview
* Provide transparent status to stakeholders
* Stay abreast of long term technical trends in the market
* Stay informed about other products' functionality and positioning
* Advocate for broad changes in the GitLab project to stay ahead of scalability and performance bottlenecks

## Must-have Requirements

* Able to enable people to go from 0 to 1: get something shipped
* Learn, live, and teach our [core values](/handbook/values/#credit)
* Be technically credible (at the VP level)
* Be an excellent recruiter of engineering talent
* Have a defined management philosophy
* Excellent verbal & written (async) communication skills
* Demonstrated longevity and perseverance at a fast-growing organization
* Be effective in an all-remote setting
* Humility

## Nice-to-have Requirements

We'd like the successful candidate to have at least a simple majority of the following attributes:

* Extensive familiarity with the GitLab project and application
* Prior success at startup or entrepreneurial organizations
* Experience at multiple product-oriented technology companies
* Experience growing a company to massive scale
* Prior developer platform or tool industry experience
* Experience working with people around the globe
* Extensive open-source work
* Demonstrate curiosity for new topics and domains
