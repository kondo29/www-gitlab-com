---
layout: markdown_page
title: "README of Katrin Leinweber"
description: "Personal readme page for Katrin Leinweber, Support Engineer, GitLab"
job: "Support Engineer (EMEA)"
---

## Katrin's README

Hi, I am Katrin! Biochemist by training, but IT generalist by choice.
Having used Git in its various forms since 2014,
I am now looking forward to diving deeper into the GitLab-surrounding technologies.

## Related

* [Team page](https://about.gitlab.com/company/team/#katrinleinweber)
* [Profile](https://gitlab.com/katrinleinweber)

## My working style

I tend to jump in quickly upon recognizing a task I can complete,
going into sleuthing-around-logs-and-code mode when necessary.

## Communicating with me

I enjoy working asynchronously via issues and MRs.
Particularly also with GitLab's `To Do` function.
Please use `@katrinleinweber` liberally.

For urgent, synchronous communication, make my Slack icon jump in macOS' Dock.
I'll be there, unless my Slack status indicates preoccupation with another urgent task.

E-mails are fine as well; I check them 2 or 3 times throughout the workday.

## Weaknesses

* Too much attention to details, up to perfectionism,
  when working on things that have no clear deadline.
* Too little stepping-back-and-seeing-the-big picture,
  when working on urgent tasks.
* Good ideas sometimes come to me only after a good night's sleep.

## Strengths

Good ideas come to me usually after a good night's sleep,
or a walk through the [great outdoors](https://www.youtube.com/watch?v=AzttaR1PIlM).

Because I worked in different sectors and industries,
and consume many different podcasts and articles,
I think I can rely on a broad base of experiences
and problem-solving approaches.
