---
layout: handbook-page-toc
title: "GitLab Ireland Ltd"
description: "Discover GitLab's benefits for team members in Ireland"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Medical

GitLab offers a private medical plan through VHI which gives you access to cutting-edge medical treatments, health screenings and protection against the high medical costs of private care. GitLab covers 100% of the premiums for team members and eligible dependents.

For a full breakdown of plan descriptions please review the following [Company Plan Plus Level 1.3 details](https://drive.google.com/file/d/1pnyWAp71MMYzdsUm1rk0o4gq7k8cdvJO/view?usp=sharing) and [Table of Benefits](https://drive.google.com/file/d/1-0cfnrKSTyQIyAEro3wR7i2aBLHsvXEv/view?usp=sharing).

To find out which hospitals and treatment centers are covered on your plan, please refer to the directories of approved facilities which are available at `Vhi.ie/downloads` or contact VHI directly via the Corporate Advice Line: `056 777 5800`.

**Does Lifetime Community Rating (LCR) Apply?**
From 1st of May 2015, if you are 35 or over when you first take out private health insurance, you will pay an additional amount of 2% for each year that you are aged 35 and over. Please contact VHI if you think this may apply.

### Enroll in the VHI Plan

GitLab's VHI scheme will begin November 1, 2020. You can choose to enroll from this date or from your hire date (if later than Nov 1st). Team members will need to enroll via VHI directly either through the online portal or by calling VHI. GitLab has selected Company Plan Plus Level 1.3 company wide. If you feel that this does not suit your needs (or family member needs), please call VHI on `056 777 5800` and they will be able to discuss your requirements.

As part of the initial enrollment, please use the following [enrollment guide](https://drive.google.com/file/d/1YwsQp7ybVZ7cYg5G4EBlPFBJskxmjfqG/view?usp=sharing) to review the plans and enroll either via the online platform or by calling VHI. Enrollment will close on October 26th for a November 1st effective date.

### Tax Implications of Medical Plans

The health insurance allowance is treated like any other income or allowance under Revenue Commissioners rules. Your normal tax rate including other statutory deductions will apply. You will be liable for Benefit in kind on your health insurance premium, for more information please read [here](https://www.revenue.ie/en/personal-tax-credits-reliefs-and-exemptions/health-and-age/medical-insurance-premiums/index.aspx). 

## Pension

GitLab offers a private pension plan via a defined contribution scheme. Orca Financial is an Irish owned Financial Services company to help with the administration of the pension on behalf of GitLab. The pension provider is **Aviva**. Aviva allows for transfers into the plan and can set up an AVC pension for any members that wish to contribute above the minimum required contribution.

Orca has put together the following [video](https://drive.google.com/file/d/1IkVkLVtKonn8JsJ2hNGZI_89V3FZyPIU/view?usp=sharing) with information about the plan as a resource.  

### Pension Match

GitLab will match up to 5% of annual base salary of the team member's contributions.

### Enrollment

To enroll in the pension plan or to make changes to an existing contribution, please email `total-rewards@gitlab.com` with the desired percent contribution. Total Rewards will add Orca Financial `info@orca.ie` to the email thread. Orca will gather all relevant information from the team member to add to the pension plan. On the 7th of every month, Orca will send Payroll and Total Rewards a summary of any changes to the pension plan which Total Rewards will update in the appropriate payroll changes spreadsheet. Total Rewards will also file the original election email in BambooHR under the "Benefits and ISO" folder for an audit trail.

After each pay cycle, Payroll will send Aviva (ccing Orca and Total Rewards) a spreadsheet with all contributions for Aviva to reconcile. 

If you have any questions about a pension plan, how pension benefits work in Ireland, or anything else, please reach out to Orca Financial who will be able to assist you directly. Email: `info@orca.ie` Phone: `+353 1 2103030`

You will be able to review the investment strategy once enrolled through the Fund centre on the Aviva website.

## Death in Service

All full-time GitLab team members in Ireland are eligible for death in service at 4x annual salary. This benefit is administered through Aviva.

## Disability

All full-time GitLab team members are eligible for disability cover in the amount of 66% of base salary less social welfare to age 65 (after 13 weeks deferred period). This benefit is administered through Aviva.

## Additional Benefits Through Aviva

* Best Doctors
* Up to 15% off Aviva general insurance products
* Free travel insurance if you take our Car & Home together

## Bike to Work/Tax Saver Commuter Scheme

GitLab uses TravelHub to assist with the administration of the bike to work/tax saver commuter scheme. The GitLab specific online portal can be found at (https://gitlab.travelhub.ie/). Team members will need to register and account to enter the GitLab specific online portal. Here is a link to TravelHub's [FAQs](https://support.hubex.ie/hc/en-ie/articles/360044040031) about these programs.  

From 2021, Cycle to work will have a maximum expenditure of 1250 EUR or 1500 EUR for an electric bike. The scheme may now be availed of once in 4 consecutive tax years, previously this was once every 5 years.


**Bike Applications:**

1. Once your account is created, select a Bike to Work Ltd partner shop. 
1. Pick the new bike and accessories if required. 
1. Upload the quote received from the bike ship to the TravelHub portal to have the order approved. 
1. The Total Rewards team will receive an email with the request to login, review, and approve the application (credentials can be found in 1password). Please note that all payments should be made to TravelHub directly and not to the supplier chosen by staff. TravelHub will pay the supplier once the employee has received their Bike or Ticket. 
1. Total Rewards will notify payroll to reduce the team member's gross salary by the cost of the bike using a Salary Sacrifice Agreement. 

**Ticket Applications:**

1. Once your account is created, select a Travel Operator. 
1. Select either a monthly or yearly ticket. 
1. Submit the travel ticket to the TravelHub portal to have the order approved. 
1. The Total Rewards team will receive an email with the request to login, review, and approve the application (credentials can be found in 1password). 
1. Total Rewards will notify payroll to reduce the team member's gross salary by the cost of the ticket using a Salary Sacrifice Agreement. 
1. Note: There is a 4% (plus VAT) booking fee for annual tickets and a 10 EUR (VAT inclusive) booking fee for monthly tickets paid for by the team member by being added to the price of the ticket. 

Once the total rewards team has approved the bike or ticket application, an invoice is generated. Once payment as been received, TravelHub emails the team member a voucher to use in the bike shop or tickets are dispatched in to the team member. 

## GitLab Ireland LTD Leave Policy

* Sick Leave

  * Team members should refer to [GitLab's PTO Policy](https://about.gitlab.com/handbook/paid-time-off/#sick-time-procedures---all-team-members) as there is no statutory requirement to provide sick pay or unpaid sick leave in Ireland. 
  * Team members must designate all time off for illness as `Out Sick` to ensure that PTO is properly tracked. 

* Maternity Leave in Ireland:
  * Team members who become pregnant in Ireland are eligible to take 26 weeks of maternity leave. Team members must take at least 2 weeks of maternity leave before your baby is due and at least 4 weeks after the baby is born. 
  * [Maternity Benefit](https://www.citizensinformation.ie/en/social_welfare/social_welfare_payments/social_welfare_payments_to_families_and_children/maternity_benefit.html#:~:text=Maternity%20Benefit%20is%20a%20payment,you%20are%20self%2Demployed) is payable by the state subject to a team member's PRSI contributions. The rate is 245 EUR per week for up to 26 weeks.
  * Team members can also claim [Parent's Benefit](https://www.citizensinformation.ie/en/social_welfare/social_welfare_payments/social_welfare_payments_to_families_and_children/parents_benefit.html) (also subject to a team member's PRSI contributions). The rate is 245 EUR per week for up to 5 weeks. This can be claimed anytime within the first year of a child's life.
  * [If you have been at GitLab for six months, GitLab will supplement the Maternity Benefit up to the full salary amount](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) for up to 16 weeks of your maternity leave.
  * Team members can take additional unpaid maternity leave for up to 16 more weeks, beginning immediately after the end of the 26 weeks' basic maternity leave. To take additional leave, please send an e-mail to Total Rewards at least 4 weeks before your basic maternity leave ends. 
  * Process for applying for state maternity benefit:
    * Notify Total Rewards of your baby's due date, the start and end dates of your maternity leave.
    * Total Rewards will send you the completed MB2 form.
    * Submit your application for state maternity benefit via the [MyWelfare website](https://services.mywelfare.ie/en/topics/parents-children-family/).



* Paternity Leave in Ireland:
  *  The Paternity Leave and Benefit Act 2016 provides two weeks' statutory leave and benefit for team members who are the relevant parents of children born or adopted.
  *  Paternity leave generally must be taken in one block of two weeks within the first six months following the birth or adoption placement. The term "relevant parent" is defined broadly to provide for fathers, adoptive fathers, civil partners, cohabitants and same-sex couples.
  *  [Paternity Benefit](https://www.citizensinformation.ie/en/social_welfare/social_welfare_payments/social_welfare_payments_to_families_and_children/paternity_benefit.html#:~:text=Paternity%20Benefit%20is%20a%20payment,or%20after%201%20September%202016) is payable by the state subject to a team member's PRSI contributions at a rate of 245 EUR per week for up to two weeks. Team members should apply for the benefit 4 weeks before they intend to go on paternity leave.
  * Team members can also claim [Parent's Benefit](https://www.citizensinformation.ie/en/social_welfare/social_welfare_payments/social_welfare_payments_to_families_and_children/parents_benefit.html) (also subject to a team member's PRSI contributions). The rate is 245 EUR per week for up to 5 weeks. This can be claimed anytime within the first year of a child's life.
  *  [If you have been at GitLab for six months, GitLab will supplement Paternity Benefit and Parent's Benefit up to the full salary amount](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) for up to 16 weeks of your parental leave.
  * Process for applying for state paternity benefit:
    * Notify Total Rewards of your baby's due date.
    * Total Rewards will send you the completed PB2 form.
    * Submit your application for state paternity benefit via the [MyWelfare website](https://services.mywelfare.ie/en/topics/parents-children-family/).

* Vacation Leave
  * Team members are entitled to a maximum of four weeks of vacation each year, which is calculated in accordance with the [Organisation of Working Time Act of 1997](http://www.irishstatutebook.ie/eli/1997/act/20/section/19/enacted/en/html#sec19). Unused vacation time may not be carried over into the following calendar year. GitLab does not provide pay in lieu of unused vacation entitlement other than on termination of employment. If your employment terminates for any reason, you will receive pay for any accrued but unused statutory vacation entitlement. Vacation leave runs concurrently with GitLab PTO. Team members must designate any vacation time taken as `Vacation` in PTO by Roots to ensure that vacation entitlement is properly tracked. 
