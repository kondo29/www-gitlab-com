---
layout: markdown_page
title: "GitLab workflow overview"
description: "GitLab supports teams across the entire software development lifecycle in a single interface"
---

With a user-friendly interface, GitLab allows software development teams to work effectively both from the command line and the UI, enabling everyone - from developers and product managers - to collaborate in a single platform. The GitLab workflow logic is intuitive and predictable, making the entire platform easy to use and easier to adopt. 

## What is the GitLab workflow?

The GitLab workflow is a logical sequence of actions taken during the software development lifecycle using GitLab as the platform to host code. The GitLab workflow takes into account the [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html), which consists of Git-based methods and tactics for version management, such as branching strategy and Git best practices.

With the GitLab workflow, the goal is to help teams work cohesively and effectively from the first stage of implementing something new (ideation) to the last stage — deploying implementation to production. 

![FROM IDEA TO PRODUCTION IN 10 STEPS](/images/blogimages/idea-to-production-10-steps.png)

### Stages of software development

The natural course of the software development process passes through 10 major steps, and GitLab has built solutions for all of them:

1. **Idea:** Every new proposal starts with an idea. 
2. **Issue:** The most effective way to discuss an idea is to create an issue for it. Team members can collaborate to polish and improve the idea in the issue tracker.
3. **Plan:** Once the discussion comes to an agreement, it's time to code. First, teams need to prioritize and organize the workflow using the [Issue Board](/blog/2018/08/02/4-ways-to-use-gitlab-issue-boards).
4. **Code:** Teams get to coding once they have everything organized.
5. **Commit:** Once teams are happy with the draft, they can commit the code to a `feature` branch with version control.
6. **Test:** With GitLab [CI](/blog/2018/01/22/a-beginners-guide-to-continuous-integration/), teams can run scripts to build and test the application.
7. **Review:** Once the script works and the tests and builds succeed, code is ready for the [code review](/blog/2020/07/03/challenges-of-code-reviews/) process.
8. **Staging:** Now it's time to deploy the code to a staging environment to check if everything worked as expected or whether adjustments are needed.
9. **Production:** When everything works as it should, it's time to deploy to the production environment.
10. **Feedback:** Teams reflect and identify what stage of work needs improvement. Teams can use Cycle Analytics for feedback on the time spent on key stages of our process.

To walk through these stages smoothly, it's important to have powerful tools to support this workflow. The following sections provide an overview of the GitLab toolset.

## GitLab Issue Tracker

GitLab has a powerful issue tracker that allows software development teams to share and discuss ideas throughout the lifecycle.

Issues are the first essential feature of the GitLab workflow. The best practice is to always start a discussion with an issue to track the evolution of an idea.

Issues are useful for:

- Discussing ideas
- Submitting feature proposals
- Asking questions
- Reporting bugs and malfunction
- Obtaining support
- Elaborating new code implementations

Each project hosted by GitLab has an issue tracker. To create a new issue, navigate to the project's issues > New issue and give it a title that summarizes the subject to be treated, and describe it using [Markdown][md-gitlab]. The [pro tips](#pro-tips) below offer ways to enhance the issue description.

The GitLab Issue Tracker offers extra functionalities to make it easier to organize and prioritize actions described in the following sections.

![new issue - additional settings](/images/blogimages/gitlab-workflow-an-overview/issue-features-view.png){:.shadow}

### Confidential issues

If developers want to contain an issue’s discussion within a team, they can make that issue confidential, so that even if the project is public, that issue will be only accessible to specific team members. The browser will respond with a 404 error if someone who is not a project member with at least reporter level tries to access that issue's URL.

### Due dates

Every issue enables developers to set a due date to it. Some teams work on tight schedules, and it's important to have a way to set up a deadline for implementations and for solving problems.

When projects have multiple due dates — for a new release, product launch, or tracking tasks by quarter — teams can use milestones.

### Assignee

Assignees can change throughout the course of the software development process. The idea is that the assignee is responsible for that issue until they reassign it to someone else to complete a different portion of the project. GitLab enables users to filter issues per assignee, allowing team leads to determine workload and find work.

### Labels

Labels are used to categorize, localize, and organize issues. Priority labels can be used to spotlight specific tasks that require additional attention, while [scoped labels](/blog/2019/06/20/issue-labels-can-now-be-scoped/) make it possible for teams to define a basic custom field that avoids confusion and cleans up issue lists. Labels enable teams to work with the Issue Board, facilitating the plan stage and organizing the workflow. Developers can create group labels to use the same labels per group of projects.

### Issue weight

An issue weight identifies the difficulty of an implementation. Less difficult tasks would receive weights of 01-03, medium difficulty should be 04-06, and more difficult tasks should be 07-09. It’s important to standardize weights according to needs so that everyone uses issue weights the same way.

### Issue Board

The Issue Board is ideal for planning and organizing issues according to a project's workflow. The board consists of lists corresponding to their respective labels, with each issue displayed as a card. The cards can be moved between lists, which automatically updates the label depending on where the issue is moved.

![GitLab Issue Board](/images/blogimages/designing-issue-boards/issue-board.gif){: .shadow}

Teams can also create issues right from the Board by clicking the <i class="fas fa-plus" style="color: green;"></i> button on the top of a list. The new issue will be automatically created with the label corresponding to that list. Multiple issue boards can be created per project to organize issues for different workflows.

![Multiple Issue Boards](/images/8_13/m_ib.gif){: .shadow}

## Code review 

It’s important for developers to commit to the code review process in order to ensure high quality code is shipped to customers.

### First commit

In the first commit message, developers can add the number of the issue related to that commit message. By doing so, they create a link between the two stages of the development workflow: the issue itself and the first commit related to that issue.

If the issue and the code are both in the same project, users can add `#xxx` to the commit message, where `xxx` is the issue number. If they are not in the same project, the full URL can be added to the issue (`https://gitlab.com/<username>/<projectname>/issues/<xxx>`). The following example shows the two options (note: replace `gitlab.com` with the URL of a team’s GitLab instance).


```shell
git commit -m "this is my commit message. Ref #xxx"
```

or

```shell
git commit -m "this is my commit message. Related to https://gitlab.com/<username>/<projectname>/issues/<xxx>"
```

Note: Linking the first commit to the issue is going to be relevant for tracking the process far ahead with GitLab Cycle Analytics. It will measure the time taken for planning the implementation of that issue, which is the time between creating an issue and making the first commit.
{: .note .alert .alert-success}

### Merge request

Once changes are pushed to a `feature` branch, GitLab will identify this change and prompt the creation of a Merge Request (MR). Every MR will have a title that summarizes the implementation and a description supported by [Markdown](/blog/2018/08/17/gitlab-markdown-tutorial/). The MR will also list any related issues and MRs (creating a link between them). Developers can add the [issue closing pattern] to close that issue once the MR is merged.

For example:

```md
## Add new page

This MR creates a `readme.md` to this project, with an overview of this app.

Closes #xxx and https://gitlab.com/<username>/<projectname>/issues/<xxx>

Preview:

![preview the new page](#image-url)

cc/ @Suri @Bree @Val
```

When creating an MR with a description like the one above, it will:

- Close both issues `#xxx` and `https://gitlab.com/<username>/<projectname>/issues/<xxx>` when merged
- Display an image
- Notify the users `@Suri`, `@Bree`, and `@Val` by e-mail

It’s best to assign the MR to the individual who will work on the project first and then re-assign it to someone else to conduct a review. The issue can be reassigned as many times as necessary to cover all the [reviews](/blog/2020/09/08/efficient-code-review-tips/) required. The MR can also be labeled and added to a milestone to facilitate organization and prioritization.

When adding or editing a file and committing to a new branch from the UI instead of from the command line, it's also easy to create a new merge request. Just mark the checkbox "start a new merge request with these changes" and GitLab will automatically create a new MR once changes are pushed.

Teams can use [Review Apps](/blog/2019/04/19/progressive-delivery-using-review-apps/) to deploy the app to a dynamic environment, from which teams can preview the changes based on the branch name, per merge request. 

![commit to a feature branch and add a new MR from the UI](/images/blogimages/gitlab-workflow-an-overview/start-new-mr-edit-from-ui.png){: .shadow}

**Note:** It's important to add the issue closing pattern to the MR in order to be able to track the process with GitLab Cycle Analytics. It will track the "code" stage, which measures the time between pushing a first commit and creating a merge request related to that commit.
{: .note .alert .alert-success}

### WIP MR

A WIP MR, which stands for Work in Progress Merge Request, which is a technique to prevent that MR from getting merged before it's ready. Just add `WIP:` to the beginning of the title of an MR, and it will not be merged until it’s removed.

When changes are ready to get merged, remove the `WIP:` pattern either by editing the issue and deleting manually or using the shortcut available just below the MR description.

![WIP MR click to remove WIP from the title](/images/blogimages/gitlab-workflow-an-overview/gitlab-wip-mr.png){:.shadow}

The `WIP` pattern can also be quickly added to the merge request with the slash command `/wip`. Simply type it and submit the comment or the MR description.

### Review

Using the diffs available on the UI, developers can create, reply to, and resolve inline comments during code reviews. This feature makes it easy to grab the link for each line of code by clicking on the line number.

The commit history is available from the UI, from which teams can track the changes between the different versions of that file and view changes inline or side-by-side.

![code review in MRs at GitLab](/images/blogimages/gitlab-workflow-an-overview/gitlab-code-review.png){: .shadow}

If there are merge conflicts, users can quickly solve them from the UI or edit the file to fix them:

![mr conflict resolution](/images/8_13/inlinemergeconflictresolution.gif){: .shadow}

## Build, test, and deploy

GitLab CI is an powerful built-in tool for continuous <a href="https://www.martinfowler.com/articles/continuousIntegration.html" target="_blank">integration</a>, continuous deployment, and continuous delivery, which can be used to run scripts. The possibilities are endless, since the command line automatically runs jobs.

The CI is set by a Yaml file called, `.gitlab-ci.yml` placed at the project's repository. CI templates can be added by simply adding a new file through the web interface and typing the file name as `.gitlab-ci.yml` to trigger a dropdown menu with dozens of possible templates for different applications.

![GitLab CI templates - dropdown menu](/images/blogimages/gitlab-workflow-an-overview/gitlab-ci-template.png){:.shadow}

### Use cases

Examples of GitLab CI use cases:

- Use it to [build][pages-post] any [Static Site Generator][ssgs-post], and deploy a website with [GitLab Pages][pages]
- Use it to [deploy a website][ivan-post] to `staging` and `production` [environments][env]
- Use it to [build an iOS application][ios-post]
- Use it to [build and deploy a Docker Image][post-docker] with [GitLab Container Registry][gcr]

Additional [GitLab CI Example Projects][ci-ex] are available to offer teams guidance. 

## Feedback: Cycle Analytics
{: #feedback}

When following the GitLab workflow, teams be able to gather feedback with GitLab Cycle Analytics on the time a team took to go from idea to production for [each key stage of the process][ca-post]:

- **Issue:** The time from creating an issue to assigning the issue to a milestone or adding the issue to a list on the Issue Board
- **Plan:** The time from giving an issue a milestone or adding it to an Issue Board list to pushing the first commit
- **Code:** The time from the first commit to creating the merge request
- **Test:** The time CI takes to run the entire pipeline for the related merge request
- **Review:** The time from creating the merge request to merging it
- **Staging:** The time from merging until deploy to production
- **Production** (total): The time it takes between creating an issue and deploying the code to production

## Enhance

### Issue and MR templates

Issue and MR templates allow teams to define context-specific templates for issue and merge request description fields for a project. These templates are written in Markdown and added to the default branch of a repository. They can be accessed by the dropdown menu whenever an issue or MR is created.

Templates save time when describing issues and MRs and standardize the information necessary to follow along. It ensures all information is provided to help everyone work as efficiently as possible.

Teams can create multiple templates to serve for different purposes. For example, teams can have one for feature proposals and a different one for bug reports. The [GitLab CE project][templates-ce] contains real examples used by the GitLab team.

![issues and MR templates - dropdown menu screenshot](/images/blogimages/gitlab-workflow-an-overview/issues-choose-template.png){:.shadow}

### Milestones

[Milestones][post-amanda] are used to track the work of a team based on a common target in a specific date.

The goal can be different for each situation, but the idea is the same: Teams have a collection of issues and merge requests being worked on to achieve that particular objective. The goal can be anything that collects the team work and effort to achieve a goal by a deadline. For example, the milestone can be to publish a new release, launch a new product, or assemble projects to get done by quarters.

At GitLab, some teams create milestones by quarter and assign every issue and MR that should be finished by the end of that quarter. Teams can also create a milestone for an event, such as an all hands meeting. By grouping issues and epics by a single goal, it’s easy to access that milestone and view an entire panorama on the progress.

![milestone dashboard](/images/blogimages/gitlab-workflow-an-overview/gitlab-milestone.png){:.shadow}

## Pro tips

### For both issues and MRs

- In issues and MRs descriptions:
  - Type `#` to trigger a dropdown list of existing issues
  - Type `!` to trigger a dropdown list of existing MRs
  - Type `/` to trigger [slash commands][slash]
  - Type `:` to trigger emojis (also supported for inline comments)
- Add images (jpg, png, gif) and videos to inline comments with the button **Attach a file**
- [Apply labels automatically][labels-post] with [GitLab Webhooks][hooks]
- Fenced blockquote: use the syntax `>>>` to start and finish a blockquote

      >>>
      Quoted text

      Another paragraph
      >>>
- Create [task lists]:

      - [ ] Task 1
      - [ ] Task 2
      - [ ] Task 3

#### Subscribe

If there’s an issue or an MR that requires follow up, it’s easy to subscribe to get notified about progress. To do so, expand the navigation on the right and click "notifications" to be updated whenever a new comment comes up. 

#### Add TO-DO

To take a future action on an issue or MR, expand the navigation tab on the right and click on "Add a to do."

#### Search for issues and MRs

When looking for an issue or MR, expand the navigation on the left and click on **Issues** or **Merge Requests** to search for ones assigned to a specific user. From there or any issue tracker, it’s easy to filter issues or MRs by author, assignee, milestone, label, and weight. Additional filters can be added to search for opened, merged, and closed.

### Moving issues

To move an issue to another project, navigate to the right and click "move issue" to transfer it to the correct project.

### Code snippets

To reuse the same code snippet or template in different projects or files, create a code snippet for future use. Expand the navigation on the left and click "Snippets" to view all saved snippets. These snippets can be public, internal, or private.

![Snippets - screenshot](/images/blogimages/gitlab-workflow-an-overview/gitlab-code-snippet.png){:.shadow}

## GitLab workflow: A use case scenario

When working within a single application, it’s easy for software development teams to collaborate. GitLab’s features help teams simplify the development process. Here’s an example of how the GitLab workflow brings teams together. 

### Labels strategy
{: .no_toc .special-h3}

As soon as a development team creates a new issue to develop a new feature to be implemented in an application, they’ll need to determine a labels strategy. For this application, teams already have created labels for "discussion," "backend," "frontend," "working on," "staging," "ready," "docs," "marketing," and "production." All of them already have their own lists in the Issue Board. The new issue currently has the label "discussion."

After the discussion in the issue tracker comes to an agreement, the backend team starts to work on that issue, so their lead moves the issue from the list "discussion" to the list "backend." The first developer who is tasked to write the code assigns the issue to herself and adds the label "working on."

### Code and commit
{: .no_toc .special-h3}

In the first commit message, the developer references the issue number. After some work, she pushes her commits to a `feature` branch and creates a new merge request, including the issue closing pattern in the MR description. The team reviews the code and makes sure all the tests and builds pass.

### Using the Issue Board
{: .no_toc .special-h3}

Once the backend team finishes their work, they remove the label "working on" and move the issue from the list "backend" to "frontend" in the Issue Board. So, the frontend team knows that issue is ready for them.

### Deploying to staging
{: .no_toc .special-h3}

When frontend developers start working on that issue, they add back the label "working on" and reassign the issue to themselves. When the issue is complete, the implementation is deployed to a staging environment. The label "working on" is removed and the issue card is moved to the "staging" list in the Issue Board.

### Collaboration across the software development lifecycle
{: .no_toc .special-h3}

Finally, when the implementation succeeds, a team moves the MR to "ready." The technical writing team collaborates to create the documentation for the new feature, and once someone gets started, they can add the label "docs." Simultaneously, the marketing team can get started on a campaign to launch and promote that feature, so someone else can add the label "marketing." When the tech writer finishes the documentation, they remove the “docs” label, and once the marketing team finishes their work, they will move the issue from "marketing" to "production."

### Deploying to production
{: .no_toc .special-h3}

At this stage, teams deploy the new feature into the production environment and the issue is closed.

### Feedback
{: .no_toc .special-h3}

With Cycle Analytics, teams can study the time taken to go from idea to production with and open an issue to discuss process improvement.

<!-- identifiers -->


[ca-post]: /2016/09/21/cycle-analytics-feature-highlight/
[env]: https://docs.gitlab.com/ee/ci/yaml/README.html#environment
[GCR]: /2016/05/23/gitlab-container-registry/
[hooks]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[ios-post]: /2016/03/10/setting-up-gitlab-ci-for-ios-projects/
[issue closing pattern]: https://docs.gitlab.com/ee/administration/issue_closing_pattern.html
[ivan-post]: /2016/08/26/ci-deployment-and-environments/
[labels-post]: /2016/08/19/applying-gitlab-labels-automatically/
[md-gitlab]: https://docs.gitlab.com/ee/user/markdown.html
[pages-post]: /2016/04/07/gitlab-pages-setup/
[pages]: https://pages.gitlab.io/
[post-amanda]: /2016/08/05/feature-highlight-set-dates-for-issues/#milestones
[post-docker]: /2016/08/11/building-an-elixir-release-into-docker-image-using-gitlab-ci-part-1/
[ssgs-post]: /2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
[task lists]: https://docs.gitlab.com/ee/user/markdown.html#task-lists
[templates-ce]: https://gitlab.com/gitlab-org/gitlab-ce/issues/new
[slash]: https://docs.gitlab.com/ee/user/project/slash_commands.html
[ci-ex]: https://docs.gitlab.com/ee/ci/examples/README.html

<style>
  .special-h3 {
    font-size: 18px !important;
    color: #444 !important;
    font-weight: 600 !important;
  }
</style>

## Want to learn more about Git and workflows?
 
* [Learn how GitLab simplifies software development](/solutions/benefits-of-using-version-control/)
* [Watch how GitLab accelerates delivery](/webcast/collaboration-without-boundaries/)
* [Download the Git branching strategies eBook to improve collaboration](/resources/ebook-git-branching-strategies/)

_Article originally written by [Marcia Ramos](/company/team/#marcia)_
