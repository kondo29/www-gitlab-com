---
layout: handbook-page-toc
title: Demand Generation
description: Demand Generation
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

---
### Demand Generation Key Metrics
{: #key-metrics .gitlab-purple}
- **North Star Metric:** MQLs
- **Efficiency Metric:** Cost per MQL
- **Business Impact Metric:** CWA/Spend (closed-won attribution)
- **Supporting/Activity Metrics:** SAOs, Attributed Pipeline, Emails Sent

## Teams
{: #teams .gitlab-purple}
- [Marketing Campaigns](/handbook/marketing/demand-generation/campaigns/)
- [Digital Marketing](/handbook/marketing/demand-generation/digital-marketing/)
- [Partner and Channel Marketing](/handbook/marketing/strategic-marketing/partner-marketing/)

[See team members in org chart](https://about.gitlab.com/company/team/org-chart/)
