---
layout: handbook-page-toc
title: "Security Awards Program"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##  Intent and goals

Security is everyone's responsibility, and therefore, everyone can contribute to it.
We want to foster a strong security culture at GitLab, and reward the people who are raising the standards and expectations.

The purpose of the Security Awards Program will be to incentivize GitLab team members, in particular Software Engineering, to solve as many security-related issues and problems as possible.

## Participation

Security is a team effort, and everyone is invited to contribute.

Are awardable:

- GitLab team members, except members of the [Security Department](https://about.gitlab.com/handbook/engineering/security/)
- Community contributors (including hackers from our bug bounty program)

The participation is purely voluntary and that there are no requirements to work extra hours or negatively impact team members’ work-life balance.
No registration is required to participate in the program, the Security Team will identify eligible actions matching the following conditions.

### Eligible actions

As everything starts with a merge request, an issue, or an epic, awarded actions must point to one of them. They must have been created or closed in the last 60 days, and
have at least the ~security label.

- Eligible example: [https://gitlab.com/gitlab-org/gitlab/-/issues/216028](https://gitlab.com/gitlab-org/gitlab/-/issues/216028) (confidential issue)
- Non Eligible example: [https://gitlab.com/gitlab-org/gitlab/-/issues/235110](https://gitlab.com/gitlab-org/gitlab/-/issues/235110) (confidential issue)

Non-exhaustive list of behaviors to incentivize:

- Defining, balancing, and implementing security requirements along with functionality
- Taking extra steps to implement a secure solution
- Finding a security risk in code (present for more than a release)
- Assisting in the building of security tooling/automation that integrates security into their work process
- Security team engagement

### Leaderboard

Every action approved will entitle the nominee for the quarterly and yearly contest.
A leaderboard is available in the [gitlab-com/security-awards](https://gitlab.com/gitlab-com/security-awards) project, and will be updated every month.
To update the leaderboard, a Merge Request must be created, pointing out the action to award.
If several actions are submitted for approval, keep in mind that not all of them could be accepted, and the merge request will have to be updated accordingly.

## Categories

Since Engineering to more like to be exposed to security topics, we want this program to be fair and make everyone feeling included and involved.
Each category below will be rewarded. We will have a limited number of prizes to distribute every quarter and year, so we want to make sure that all categories are well represented.
That's why we will make sure to reward a certain number of slots in each category below:

### Development

Engineers and Managers from the [Development Department](https://about.gitlab.com/handbook/engineering/development/).

### Engineering (except Development)

Rest of Engineering: QA, Support, Product Design, Infrastructure, ...

### Non-Engineering

Rest of the company: Marketing, Product, Sales, Legal, PeopleOps, ...

### Community contributions

Only security fixes and contributions from the community will be considered.  
We already have a [Bug Bounty Program](https://hackerone.com/gitlab) for external contributors to report security issues and bugs. 

## Prizes

### Quarterly contest

At the end of every [fiscal quarters](/handbook/finance/#fiscal-year), the top nominees for each category will be able to redeem their prizes.

#### FY21-Q4 prizes

| Category        | Rank | Prize                  |
|-----------------|-----:|------------------------|
| Development     |  1-5 | $100 Amazon Gift Card  |
| Development     | 6-25 | $20 Amazon Gift Card   |
| Engineering     |  1-5 | $20 Amazon Gift Card   |
| Non-Engineering |  1-5 | $20 Amazon Gift Card   |
| Community       |    1 | $100 Amazon Gift Card  |

### Yearly contest

The leaderboard will keep a ranking of all participants for 4 consecutive quarters.
At the end of the fiscal year, the top 1 winners of each category will be able to redeem a bigger prize.
The Yearly Contest will start with the FY22 only.

### Ties

In the case where two or more nominees would be in a tie for a place rewarded with a prize, a raffle will be set-up to decide the winners. The raffle will use the [wheel of names](https://wheelofnames.com/) in a live-streamed session, hosted by the [Security Engineering and Research Director](/job-families/engineering/security-management/#director-of-security).

## How to award people?

Anyone in the Security Department can nominate someone by using the label `~security-awards::nomination` on an issue or a merge request in the
[gitlab.com/gitlab-org](https://gitlab.com/gitlab-org) or [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com) groups.
The nominations will be reviewed every week by the Security Engineers. 

## Process

### Labels and triaging

The following [scoped](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels-premium) labels are being used in this process:

- `~security-awards::nomination`
- `~security-awards::rejected`
- `~security-awards::awarded`

Every week, an Engineer designated in the [Triage Rotation spreadsheet](https://docs.google.com/spreadsheets/d/18vz84dgTfetTaBjbOCXaLKNfzLYMiy_tBW6RfEUYYHk/edit#gid=0)
will be in charge of gathering all nominated issues, merge requests, and epics, at the beginning of the rotation.

A new issue will be created by this Engineering in the [Security Meta project](https://gitlab.com/gitlab-com/gl-security/security-department-meta/) with the Title:

`Security Awards Program Council Discussion week of [date]`

A reminder will be sent on Slack, in the `#sec-appsec` channel to remind everyone to vote for their favorite nominations.

Every nomination is added as a threaded comment. Approval will be indicated by 👍 from 2 team members. The number of votes (👍 ) will define the number of points to attribute to a nomination: 100 points per vote will be rewarded. 

At the end of the week, the rotation Engineer will create the merge requests in the [gitlab-org/security-awards](https://gitlab.com/gitlab-com/security-awards) project 
(see this example [MR](https://gitlab.com/gitlab-com/security-awards/-/merge_requests/1)).
The rotation Engineer has the responsibility to apply the next workflow label, either `~security-awards::rejected` (if merge request closed) or `~security-awards::awarded` when creating these merge requests.

## Measurement of success

- Number of issues/MRs labelled with ~security-awards::nominations vs ~security-awards::awarded
- Total number of nominees in each category
- Total number of security issues resolved by severity
- Lines of security documentation improved
