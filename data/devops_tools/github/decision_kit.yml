header: includes/devops-tools/headers/no-button.html.haml
description: GitHub vs GitLab feature comparisons, roadmap comparisons, licensing, support and other detailed information.
strips:
  - template: markdown
    content:
      outer_background: white
      inner_background: grey
      text: |
        ## GitHub Universe 2020

        From December 8th thru December 10th, GitHub hosted GitHub Universe 2020. During this event they announced and showcased a number of new features.

        **Some of the notable features they announced were:**

          * The introduction of Dark Mode
          * Auto-merge pull requests when using protected branches
          * Environments to protect your app, package, or website with rules and environment-specific secrets
          * Required reviewers for Actions for deployment to environments that need more control

        GitHub's Enterprise Server 3.0 will introduce features such as Code Scanning and Packages to be supported for on-premise deployments.

        **A few beta features announced included:**

          * GitHub Discussions
          * Dependency Review
          * Secret Scanning (GitHub Enterprise Server 3.0)

        We will update this Decision Kit with GitHub's beta features as they move to general release.

        Explore this GitLab vs. GitHub Decision Kit for more details on how GitLab compares to GitHub considering GitHub's most recent feature updates.

  - template: multi-tab
    content:
      background: grey
      title: GitHub v/s GitLab <br> Deep Insights
      text: Explore these links for deeper insights on how GitHub and GitLab compare.  <br> - Side by side comparisons <br> - GitHub Actions as articulated by GitHub users <br> - and much more.
      tabs:
        - tab: CI-CD
          items:
            - text: GitLab CI vs GitHub Actions
              subtext: Continuous Integration comparison, highlighting gaps and challenges in GitHub Actions.
              link: /devops-tools/github-vs-gitlab/ci-missing-github-capabilities/
            - text: GitLab vs. GitHub for Continuous Delivery
              subtext: Continuous Delivery comparison, highlighting gaps and challenges in GitHub Actions.
              link: /devops-tools/github-vs-gitlab/cd-missing-github-capabilities/
            - text: CI-CD Roadmap Comparison
              subtext: Compares CI-CD capability and highlights capability present in GitLab, that is not yet reflected in the GitHub's roadmap.
              link: /devops-tools/github-vs-gitlab/ci-cd-roadmap-comparison/
            - text: GitLab vs GitHub Insights and Security for Open Source Dependencies
              subtext: Compares how GitLab and GitHub provide insights and security for projects that rely on open source dependencies.
              link: /devops-tools/github-vs-gitlab/github-vs-gitlab-dependencies/

        - tab: DevSecOps
          items:
            - text: GitLab vs. GitHub for DevSecOps
              subtext: DevSecOps comparison, highlighting gaps and challenges in GitHub's security capabilities.
              link: /devops-tools/github-vs-gitlab/devsecops-missing-github-capabilities/
            - text: Securing Your DevOps Process
              subtext: GitHub missing capabilities in Securing DevOps & Compliance.
              link: /devops-tools/github-vs-gitlab/securing-devops/
            - text: DevSecOps Roadmap Comparison
              subtext: Compares Security capability and highlights capability present in GitLab, that is not yet reflected in the GitHub's roadmap.
              link: /devops-tools/github-vs-gitlab/security-roadmap-comparison/
        - tab: VCC
          items:
            - text: Version Control & Collaboration
              subtext: Highlights VC & C features that are in GitLab but missing in GitHub
              link: /devops-tools/github-vs-gitlab/vcc-missing-github-capabilities/
            - text: Version Control Roadmap Comparison
              subtext: Compares VC & C capability and highlights capability present in GitLab, that is not yet reflected in the GitHub's roadmap.
              link: /devops-tools/github-vs-gitlab/vcc-roadmap-comparison/
        - tab: License & Other
          items:
            - text: GitLab vs GitHub For Business Decision Makers
              subtext: A brief summary of comparison analysis including GitHub strengths, limitations, challenges and GitLab's key differentiators.
              link: /devops-tools/github-vs-gitlab/business-decision-makers/
            - text: GitLab vs GitHub License Comparison
              subtext: GitLab delivers superior value with each license tier we offer.  Reveiw this license comparison between GitLab and GitHub.  Understand the gaps and risk associated with GitHub license offerings.
              link: /devops-tools/github-vs-gitlab/license-comparison/
            - text: GitLab vs GitHub Support Comparison
              subtext: Compares GitLab's and GitHub's support solutions, highlighting GitHub's support gaps.
              link: /devops-tools/github-vs-gitlab/support-comparison/
            - text: GitHub News
              subtext: Key milestones and news announcements by GitHub.
              link: /devops-tools/github-vs-gitlab/github-news/
  - template: standard-img
    content:
      image: /images/devops-tools/money-scale.png
      image_position: left
      background: white
      title: GitHub vs GitLab License Comparison
      text: |
        GitLab delivers superior value with each license tier we offer.  Reveiw this license comparison between GitLab and GitHub.  Understand the gaps and risk associated with GitHub license offerings.
      link_name: License Comparison
      link_url: /devops-tools/github-vs-gitlab/license-comparison/
